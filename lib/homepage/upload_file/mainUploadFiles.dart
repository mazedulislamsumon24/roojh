import 'dart:io';
import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:roojh/common_code/footer.dart';

import '../../FirebaseAuth/firebase_storage/firebase_storage.dart';
import '../../common_code/profileTopImage.dart';
import '../../reports_page/reports_page.dart';
import '../../showFiles/showFile_main.dart';

// #######################################
// main upload file page where you will upload pdf from library and capture image to upload firebase storage
class MainUploadFiles extends StatefulWidget {
  const MainUploadFiles({
    Key? key,
  }) : super(key: key);

  @override
  State<MainUploadFiles> createState() => _MainUploadFilesState();
}

List<String> months = [
  '',
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

class _MainUploadFilesState extends State<MainUploadFiles> {
  FilePickerResult? getFile;
  final user = FirebaseAuth.instance.currentUser; // get user details
  FirebaseStorage _storageRef =
      FirebaseStorage.instance; //firebase storage intance
  // ##############################
  // this function provide to get image by capture through mobile camera
  late File _image;
  Future getImage() async {
    var image = await ImagePicker().pickImage(source: ImageSource.camera);
    // if image is not selected then it will return null
    if (image == null) return;
    // ###########################
    // if image captured
    var filename = image.name; //get image name
    // print('file name--- $filename');
    // final imageTemporary = await File(image.path);
    // print('file name--- $imageTemporary');
    // setState(() {
    //   // this._image = imageTemporary;
    // });

    if (image != null) {
      // ############################
      //upload image function called after capturing image
      await uploadImage(image);
    }
  }

// ##############################
// this function upload the image after capturing the image
  Future<void> uploadImage(XFile _image) async {
    Reference reference =
        _storageRef.ref().child('${user?.email}').child(_image.name);
    UploadTask uploadTask = reference.putFile(File(_image.path));
    // ################################################
    // it will show uploading notification on screen
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
          backgroundColor: Colors.black,
          duration: Duration(seconds: 5),
          content: Text('Uploading')),
    );
    // ##################################
    // when upload is successful then it will show a notification that upload successful
    await uploadTask.whenComplete(() async {
      DateTime? getdate = DateTime.now();
      String stringDate = "${getdate.year}";
      String stringYear = "${getdate.year}";
      int IntMonth = (getdate.month);
      String? imgUrl = (await uploadTask.storage
          .ref()
          .child('${user?.email}')
          .child('/${reference.name}')
          .getDownloadURL());
      print('url________________________$imgUrl');
      FireStoreDatabase()
          .users
          .doc(user!.email)
          .collection('documents')
          .doc('${getdate}')
          .set({
        'description': '',
        'status': 'new',
        'summary': '',
        'imgUrl': imgUrl,
        'pdfUrl': '',
        'date': getdate,
        'month': months[IntMonth],
        'year': getdate.year.toString(),
        'documentType': 'Image',
        'email': user?.email,
        'uid': user?.uid
      });

      print(reference.getDownloadURL());
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            backgroundColor: Colors.green,
            duration: Duration(seconds: 7),
            content: Text('Upload Successful')),
      );
    });
  }

// For pdf upload
// ############################
  // get current user details
  final auth = FirebaseAuth.instance.currentUser;
  // loading indicator start from 0
  double progress = 0.0;
// #####################
// for selected files names
  List<File>? fileNames;
// #####################
// for selected files path
  List<File>? filePaths;

// #################################
// get file from test form and upload in firebase storage
  Future<String?> getPdf() async {
    // print('file name-0000 ${widget.fileNames!.first}');

    UploadTask? task = FirebaseStorage.instance
        .ref()
        .child('${auth?.email}')
        .child('/${fileNames!.first.toString()}')
        .putData(filePaths!.first.readAsBytesSync());
    print('--------------------$task');
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
          backgroundColor: Colors.black,
          duration: Duration(seconds: 3),
          content: Text('Uploading File')),
    );
    task.snapshotEvents.listen((event) {
      setState(() {
        progress =
            ((event.bytesTransferred.toDouble() / event.totalBytes.toDouble()) *
                    100)
                .roundToDouble();

        print(progress);
      });
    });

    // #############################
    // when upload complete the information of file will save in firebase database
    await await task.whenComplete(() async {
      DateTime? getdate = DateTime.now();
      String stringYear = "${getdate.year}";
      int IntMonth = (getdate.month);
      String? pdfUrl = (await task.storage
          .ref()
          .child('${auth?.email}')
          .child('/${fileNames!.first.toString()}')
          .getDownloadURL());
      print('url________________________$pdfUrl');
      // ignore: avoid_single_cascade_in_expression_statements
      FireStoreDatabase()
          .users
          .doc(auth!.email)
          .collection('documents')
          .doc('${getdate}')
          .set({
        'description': '',
        'status': 'new',
        'summary': '',
        'pdfUrl': pdfUrl,
        'imgUrl': '',
        'date': getdate,
        'month': months[IntMonth],
        'year': getdate.year.toString(),
        'documentType': 'PDF',
        'email': auth?.email,
        'uid': auth?.uid,
      });
    });
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
          backgroundColor: Colors.green,
          duration: Duration(seconds: 7),
          content: Text('Uploaded Successfully')),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: [
          TopProfileImage(), //it wil show user picture and name which is in common code folder
          Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 47, right: 47),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    'Please upload your document to get\n             your dashboard ready',
                    style: TextStyle(
                      decorationStyle: TextDecorationStyle.double,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Center(
                child: Container(
                  width: 400,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // ############################
                      // File upload button
                      TextButton(
                          onPressed: () async {
                            FilePickerResult? getFile =
                                await FilePicker.platform.pickFiles(
                                    allowMultiple: false,
                                    type: FileType.custom,
                                    allowedExtensions: ['pdf']);
                            print(getFile);
                            setState(() {
                              filePaths = getFile!.paths
                                  .map((path) => File(path!))
                                  .toList();
                              // fileName2 = await getFile!.files.first.name; //test
                              fileNames = getFile.names
                                  .map((name) => File(name!))
                                  .toList();
                              // file = await File(filePath!);
                            });
                            await getPdf();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Footer(
                                      landingIndex: 4,
                                    )));
                          },
                          child: SvgPicture.asset('icons/fileupload2.svg')),
                      // ############################
                      // Capture image button
                      TextButton(
                          onPressed: () async {
                            await getImage(); //calling getimage function which will capture image and ulpad to storage

                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Footer(
                                      landingIndex: 4,
                                    )));
                          },
                          child: SvgPicture.asset('icons/uploadImage.svg'))
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 129,
              ),
              Container(
                height: 51.8,
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  onPressed: () async {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Footer(
                              landingIndex: 3,
                            )));
                  },
                  child: const Text('Reports',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color.fromRGBO(244, 101, 36, 10))),
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.black),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(HexColor('#FEF0E9')),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.9),

                        // side: BorderSide(color: Colors.red)
                      ))),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
