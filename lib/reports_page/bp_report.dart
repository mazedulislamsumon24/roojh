import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';

class BpReport extends StatefulWidget {
  String filterValue;
  String zoomValue;

  BpReport({Key? key, required this.filterValue, required this.zoomValue})
      : super(key: key);

  @override
  State<BpReport> createState() => _BpReportState();
}

class _BpReportState extends State<BpReport> {
  String? filterValue;
  String? zoomValue;
  User? user = FirebaseAuth.instance.currentUser;
  List<_ChartData>? chartData = <_ChartData>[];
  List sysData = [];
  List diaData = [];
  double max_value = 200;

  @override
  void initState() {
    filterValue = widget.filterValue;
    zoomValue = widget.zoomValue;
    getDataFromFireStore();
    super.initState();
  }

  // ignore: non_constant_identifier_names
  DateTime todaysDate = DateTime.now();

  Map<String, double> zoomValue_map = {'100%': 0, '150%': 100, '200%': 200};
  Map<String, double> x_axis_interval = {
    'D': 0.1,
    'W': 0.05,
    'M': 0.08,
    '6M': 0.1,
    'Y': 0.1
  };
  Map<String, int> days_less = {'D': 1, 'W': 7, 'M': 31, '6M': 180, 'Y': 365};
  Map<String, dynamic> date_format = {
    'D': DateFormat.jm(),
    'W': DateFormat.E(),
    'M': DateFormat.MMMd(),
    '6M': DateFormat.MMM(),
    'Y': DateFormat.MMM()
  };

  Future<void> getDataFromFireStore() async {
    DateTime todaysDate_min =
        DateTime.now().subtract(Duration(days: days_less[filterValue] ?? 0));
    ;

    Timestamp timestamp = Timestamp.fromDate(todaysDate_min);
    var snapShotsValue = await FirebaseFirestore.instance
        .collection("users")
        .doc('${user!.email}')
        .collection('vitals')
        .doc('blood_pressure')
        .collection('blood_pressure')
        .where('date', isGreaterThanOrEqualTo: timestamp)
        .orderBy('date', descending: false)
        .get();

    List<_ChartData> list = snapShotsValue.docs
        .map((e) => _ChartData(
            time: e.data()['date'], sys: e.data()['sys'], dia: e.data()['dia']))
        .toList();
    setState(() {
      chartData = list;
    });
    // for only systolic data
    for (var a in chartData!) {
      var systolic = a.sys;
      sysData.add(systolic);
    }

    sysData.sort();

    // for only diatolic data
    for (var a in chartData!) {
      var diatolic = a.dia;
      diaData.add(diatolic);
    }

    diaData.sort();
  }

// Class for chart data source, this can be modified based on the data in Firestore

  Widget _buildLineChart() {
    //max_value =  sysData.last <= 200 ? 150 :250;

    return SfCartesianChart(
      // plotAreaBorderWidth: 0,
      title: ChartTitle(text: ''),

      primaryXAxis: DateTimeAxis(

          // to fix the crop of initial and ending value we must implement solution mentioned here. But to do this we need to have some solid data, hence parking this
          // https://www.syncfusion.com/forums/160997/columnseries-column-is-cropped-in-half-at-the-start-and-end-of-the-datetime-axis

          dateFormat: date_format[filterValue],
          isVisible: true,
          edgeLabelPlacement: EdgeLabelPlacement.shift,
          interval: x_axis_interval[filterValue],
          visibleMinimum: todaysDate
              .subtract(Duration(days: days_less[filterValue] ?? 0))
              .add(Duration(hours: days_less[filterValue] ?? 0)),
          //visibleMinimum: todaysDate.subtract(Duration(hours: 17)) ,
          visibleMaximum: todaysDate
              .add(Duration(hours: (days_less[filterValue] ?? 0)) ~/ 2),
          //visibleMaximum: todaysDate.add(Duration(hours: 7)),
          //visibleMinimum: timestamp,

          //intervalType:'IntervalType.Years',
          //labelFormat: 'yMMM',

          majorGridLines: const MajorGridLines(width: 0.6)),

      primaryYAxis: NumericAxis(
        majorGridLines: MajorGridLines(
            width: 0.1, color: Color.fromARGB(255, 115, 114, 114)),
        interval: 50,
        rangePadding: ChartRangePadding.round,
        //maximum: 150,
        minimum: 50,
        axisLine: const AxisLine(
          width: 0,
        ),
        majorTickLines: const MajorTickLines(color: Colors.transparent),
      ),
      series: <ChartSeries<_ChartData, Timestamp>>[
        ScatterSeries<_ChartData, Timestamp>(
            animationDuration: 1500,
            color: Color(0XFFF46524),
            dataSource: chartData!,
            xValueMapper: (_ChartData data, _) => data.time,
            yValueMapper: (_ChartData data, _) => data.dia,
            pointColorMapper: (_ChartData data, _) =>
                Color.fromARGB(255, 85, 83, 83),

            // width: 2,
            name: 'DIA',
            markerSettings: MarkerSettings(
              height: 9.5 + (zoomValue_map[zoomValue] ?? 0) / 50,
              width: 9.5 + (zoomValue_map[zoomValue] ?? 0) / 50,
              shape: DataMarkerType.diamond,
            )),
        ScatterSeries<_ChartData, Timestamp>(
            animationDuration: 1500,
            color: Color(0XFF204289),
            dataSource: chartData!,
            pointColorMapper: (_ChartData data, _) =>
                Color.fromARGB(255, 109, 150, 217),

            // width: 2,
            name: 'SYS',
            xValueMapper: (_ChartData data, _) => data.time,
            yValueMapper: (_ChartData data, _) => data.sys,
            markerSettings: MarkerSettings(
              height: 9 + (zoomValue_map[zoomValue] ?? 0) / 50,
              width: 9 + (zoomValue_map[zoomValue] ?? 0) / 50,
              shape: DataMarkerType.circle,
            ))
      ],
      tooltipBehavior: TooltipBehavior(enable: true, format: 'point.y '),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.6),
            spreadRadius: 8,
            blurRadius: 6,
            offset: Offset(5, 5),
          ),
        ],
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          Row(
            children: const [
              Icon(Icons.monitor_heart, color: Colors.red),
              Text("Blood Pressure"),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // const Text(
              //   "Ask the client to update the value",
              //   style: TextStyle(
              //       color: Colors.blueAccent,
              //       decoration: TextDecoration.underline),
              // ),
              const SizedBox(
                height: 5,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    width: 30,
                  ),
                  Column(
                    children: [
                      Row(
                        children: const [
                          Icon(
                            Icons.circle,
                            color: Color.fromARGB(255, 109, 150, 217),
                            size: 10,
                          ),
                          Text(" SYSTOLIC", style: TextStyle(fontSize: 12)),
                        ],
                      ),
                      Container(
                        child: sysData.length >= 2
                            ? Text(
                                "${sysData.first} - ${sysData.last}",
                                style: TextStyle(fontSize: 14),
                              )
                            : Text(
                                "__ - __",
                                style: TextStyle(fontSize: 14),
                              ),
                      )
                    ],
                  ),
                  const SizedBox(
                    width: 50,
                  ),
                  Column(
                    children: [
                      Row(
                        children: const [
                          Icon(
                            Icons.square,
                            color: Color.fromARGB(255, 85, 83, 83),
                            size: 10,
                          ),
                          Text(" DISATOLIC", style: TextStyle(fontSize: 12)),
                        ],
                      ),
                      Container(
                        child: diaData.length >= 2
                            ? Text(
                                "${diaData.first} - ${diaData.last}",
                                style: TextStyle(fontSize: 14),
                              )
                            : Text(
                                "__ - __",
                                style: TextStyle(fontSize: 14),
                              ),
                      )
                    ],
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Column(
                    children: const [
                      Text(""),
                      Padding(
                        padding: EdgeInsets.only(top: 1.0),
                        child: Text("mmHg", style: TextStyle(fontSize: 14)),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          FittedBox(
            child: SizedBox(
              child: _buildLineChart(),
              height: 150 + (zoomValue_map[zoomValue] ?? 0),
              width: 400,
            ),
          ),
          // SizedBox(
          //   height: 20,
          //   width: 250,
          //   child: ElevatedButton(
          //     style: ElevatedButton.styleFrom(
          //       padding: EdgeInsets.all(1),
          //       primary: Color(0XFF16B25E),
          //       shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(21.0),
          //       ),
          //     ),
          //     onPressed: () {},
          //     child: Text(
          //       "Add to dashboard",
          //       style: TextStyle(color: Colors.white, fontSize: 12),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}

class _ChartData {
  _ChartData({this.sys, this.dia, this.time});

  final int? sys;
  final int? dia;
  final Timestamp? time;
}
