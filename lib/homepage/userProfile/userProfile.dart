// ignore_for_file: deprecated_member_use

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  final user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,

        leading: IconButton(
          onPressed: () {
            if (Navigator.canPop(context)) {
              Navigator.pop(context);
            } else {
              SystemNavigator.pop();
            }
          }, // Handle your on tap here.
          icon: SvgPicture.asset('icons/arrow - right.svg'),
        ),
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          "Profile",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                Container(
                  height: 160,
                  decoration: BoxDecoration(
                      color: HexColor('#E4EBFF'),
                      borderRadius: BorderRadius.only(
                        // topLeft: Radius.circular(-40.89),
                        bottomLeft: Radius.circular(260.89),
                        bottomRight: Radius.circular(170),
                      )),
                ),
                Positioned(
                    top: 80,
                    left: 120,
                    child: SizedBox(
                      width: 128,
                      height: 128,
                      child: CircleAvatar(
                        radius: 66,
                        backgroundImage: NetworkImage('${user!.photoURL}'),
                      ),
                    )),
                Positioned(
                  top: 200,
                  left: 130,
                  child: Container(
                    width: 300,
                    height: 64,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '${user!.displayName}',
                            // 'sahil kumar gupta',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w700),
                          ),
                          TextButton(
                              onPressed: () {},
                              child: SvgPicture.asset('icons/pencil.svg'))
                        ],
                      ),
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text('25 Year ,Gender'))
                    ]),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 150,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: [
                  Container(
                    height: 75,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: HexColor('#E4EBFF'),
                        borderRadius: BorderRadius.all(Radius.circular(9))),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Family\nmember',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                              width: 168,
                              height: 44,
                              child: Image.asset(
                                'icons/family.png',
                              ))
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Center(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 46,
                              width: 150,
                              child: TextButton(
                                  onPressed: () {},
                                  child: const Text('Basic',
                                      style: TextStyle(
                                          fontSize: 17, color: Colors.white)),
                                  style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              HexColor('#F46524')),
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(22),
                                        ),
                                      )))),
                          Container(
                            height: 46,
                            width: 150,
                            child: TextButton(
                                onPressed: () {},
                                child: const Text('Advance',
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white)),
                                style: ButtonStyle(
                                    foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.black),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            HexColor('#F46524')),
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(22),
                                      ),
                                    ))),
                          )
                        ]),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  ProfileForm()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ProfileForm extends StatefulWidget {
  const ProfileForm({Key? key}) : super(key: key);

  @override
  State<ProfileForm> createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  // Default Radio Button Selected Item When App Starts.
  String radioButtonItem = 'ONE';
  Color radio_color = HexColor('#204289');

  // Group Value for Radio Button.
  int id = 1;
  @override
  Widget build(BuildContext context) {
    return Form(
        child: Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, bottom: 2),
            child: Text(
              'Age',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        TextFormField(
          obscureText: true,
          enableSuggestions: false,
          autocorrect: false,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
          decoration: InputDecoration(
            filled: true,
            fillColor: HexColor('#F3F6FF'),
            border: InputBorder.none,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Age",
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(98.67),
              borderSide: BorderSide(
                color: HexColor('#CED3E1'),
                width: 1.0,
              ),
            ),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(98.67),
                borderSide: BorderSide(
                  color: HexColor('#CED3E1'),
                  width: 1.0,
                )),
            errorStyle: TextStyle(color: Colors.red, fontSize: 15),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(98.67),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(98.67),
            ),
          ),
          // controller: confirm_passwordController,
          onChanged: (val) {
            setState(() {
              // conformpass = val;
            });
          },
        ),
        SizedBox(
          height: 20,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 14.0),
            child: Text(
              'Gender',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.only(left: 11),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                visualDensity: const VisualDensity(
                    horizontal: VisualDensity.minimumDensity,
                    vertical: VisualDensity.minimumDensity),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                activeColor: HexColor('#204289'),
                focusColor: HexColor('#204289'),
                value: 1,
                groupValue: id,
                onChanged: (val) {
                  setState(() {
                    radioButtonItem = 'ONE';
                    id = 1;
                  });
                },
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'Male',
                style:
                    new TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                width: 25,
              ),
              Radio(
                visualDensity: const VisualDensity(
                    horizontal: VisualDensity.minimumDensity,
                    vertical: VisualDensity.minimumDensity),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                activeColor: HexColor('#204289'),
                focusColor: HexColor('#204289'),
                value: 2,
                groupValue: id,
                onChanged: (val) {
                  setState(() {
                    radioButtonItem = 'TWO';
                    id = 2;
                  });
                },
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'Female',
                style:
                    new TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, bottom: 2),
                      child: Text(
                        'Height',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 54,
                    child: TextFormField(
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: HexColor('#F3F6FF'),
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "OFT",
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(98.67),
                          borderSide: BorderSide(
                            color: HexColor('#CED3E1'),
                            width: 1.0,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(98.67),
                            borderSide: BorderSide(
                              color: HexColor('#CED3E1'),
                              width: 1.0,
                            )),
                        errorStyle: TextStyle(color: Colors.red, fontSize: 15),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(98.67),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(98.67),
                        ),
                      ),
                      // controller: confirm_passwordController,
                      onChanged: (val) {
                        setState(() {
                          // conformpass = val;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 19.0),
              child: Container(
                // padding: EdgeInsets.only(top: 20),
                width: 101,
                height: 54,
                child: TextFormField(
                  obscureText: true,
                  enableSuggestions: false,
                  autocorrect: false,
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: HexColor('#F3F6FF'),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "0.0 IN",
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(98.67),
                      borderSide: BorderSide(
                        color: HexColor('#CED3E1'),
                        width: 1.0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(98.67),
                        borderSide: BorderSide(
                          color: HexColor('#CED3E1'),
                          width: 1.0,
                        )),
                    errorStyle: TextStyle(color: Colors.red, fontSize: 15),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(98.67),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(98.67),
                    ),
                  ),
                  // controller: confirm_passwordController,
                  onChanged: (val) {
                    setState(() {
                      // conformpass = val;
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 4),
              child: Container(
                  height: 52,
                  width: 52,
                  child: TextButton(
                      onPressed: () {},
                      child: const Text('IN',
                          style: TextStyle(fontSize: 17, color: Colors.white)),
                      style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.black),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              HexColor('#204289')),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(127),
                            ),
                          )))),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 2),
              child: Container(
                height: 52,
                width: 52,
                child: TextButton(
                    onPressed: () {},
                    child: const Text('CM',
                        style: TextStyle(fontSize: 17, color: Colors.white)),
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            HexColor('#204289')),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(127),
                          ),
                        ))),
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, bottom: 2),
                      child: Text(
                        'Weight',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 54,
                    child: TextFormField(
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: HexColor('#F3F6FF'),
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Weight",
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(98.67),
                          borderSide: BorderSide(
                            color: HexColor('#CED3E1'),
                            width: 1.0,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(98.67),
                            borderSide: BorderSide(
                              color: HexColor('#CED3E1'),
                              width: 1.0,
                            )),
                        errorStyle: TextStyle(color: Colors.red, fontSize: 15),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(98.67),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(98.67),
                        ),
                      ),
                      // controller: confirm_passwordController,
                      onChanged: (val) {
                        setState(() {
                          // conformpass = val;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 4),
              child: Container(
                  height: 52,
                  width: 52,
                  child: TextButton(
                      onPressed: () {},
                      child: const Text('KG',
                          style: TextStyle(fontSize: 17, color: Colors.white)),
                      style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.black),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              HexColor('#204289')),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(127),
                            ),
                          )))),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 2),
              child: Container(
                height: 52,
                width: 52,
                child: TextButton(
                    onPressed: () {},
                    child: const Text('LB',
                        style: TextStyle(fontSize: 17, color: Colors.white)),
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            HexColor('#204289')),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(127),
                          ),
                        ))),
              ),
            )
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12.0, left: 2),
          child: Container(
            height: 52,
            width: double.infinity,
            child: ElevatedButton(
                onPressed: () {},
                child: const Text('Save',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(HexColor('#F46524')),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                    ))),
          ),
        ),
        SizedBox(height: 30)
      ],
    ));
  }
}
