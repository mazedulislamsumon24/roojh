import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/common_code/footer.dart';

class CommonCardForm extends StatefulWidget {
  String? title;
  String? valuesUnit;
  String vital_db;
  CommonCardForm(
      {Key? key,
      required this.title,
      required this.valuesUnit,
      required this.vital_db})
      : super(key: key);

  @override
  State<CommonCardForm> createState() => _CommonCardFormState();
}

class _CommonCardFormState extends State<CommonCardForm> {
  final _formKey = GlobalKey<FormState>();
  final valueTextCon = TextEditingController();
  String? user = FirebaseAuth.instance.currentUser!.email;
  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2020, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      // ignore: curly_braces_in_flow_control_structures
      setState(() {
        selectedDate = picked;
      });
  }

  Future<void> updateTiles(int value) async {
    FirebaseFirestore.instance
        .collection('users')
        .doc('${user}')
        .collection('tiles')
        .doc(widget.vital_db)
        .update({'value': value.toString()});
  }

  Future<void> addUser(int date, int month, int value) {
    return FirebaseFirestore.instance
        .collection('users')
        .doc('${user}')
        .collection('vitals')
        .doc('${widget.vital_db}')
        .collection('${widget.vital_db}')
        .add({'date': selectedDate, 'value': value}).then((val) async {
      // update tiles function call
      await updateTiles(value);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Colors.green,
          duration: Duration(seconds: 5),
          content: Text('Data Submited Successfully'),
        ),
      );
      // await Future.delayed(const Duration(seconds: 2));
      await Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => Footer(
                landingIndex: 3, // 3 denotes to show reports page
              )));
      // ignore: invalid_return_type_for_catch_error
    }).catchError((error) => ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('Something Went Wrong'),
              ),
            ));
  }

  final auth = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: Drawer(),
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            if (Navigator.canPop(context)) {
              Navigator.pop(context);
            } else {
              SystemNavigator.pop();
            }
          }, // Handle your on tap here.
          icon: SvgPicture.asset('icons/arrow - right.svg'),
        ),
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyRadiob object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          "${widget.title}",
          style: TextStyle(
            color: Colors.black.withOpacity(0.8),
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Container(
                      child: Column(children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              '${widget.title}',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.red.shade600),
                            )),
                        const SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 100,
                              child: TextFormField(
                                // obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                                style: const TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w600),
                                decoration: InputDecoration(
                                  hintText: "60",
                                  //create lable
                                  hintStyle: TextStyle(
                                      color: Colors.black.withOpacity(0.2)),
                                  filled: true,
                                  fillColor: HexColor('#F3F6FF'),
                                  // border: InputBorder.none,
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.0)),
                                  ),
                                  contentPadding: const EdgeInsets.fromLTRB(
                                      30.0, 15.0, 20.0, 0),

                                  // hintText: "120",
                                ),
                                controller: valueTextCon,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'please enter Systolic Value';
                                  }
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 32, left: 10),
                              child: Text(
                                '${widget.valuesUnit}',
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.5)),
                              ),
                            )
                          ],
                        )
                      ]),
                      padding: const EdgeInsets.all(22),
                      height: 165,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: HexColor('#F3F6FF'),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(23)),
                          boxShadow: const [
                            // BoxShadow(blurRadius: 1.0),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: Offset(0, -5)),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: (Offset(5, 0)))
                          ]),
                    ),
                  ),
                  const SizedBox(
                    height: 150,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      height: 51,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: HexColor('#F3F6FF'),
                        borderRadius: BorderRadius.circular(98),
                        border: Border.all(color: HexColor('#CED3E1')),
                        //   boxShadow: [
                        //     BoxShadow(
                        //       color: Color(0xffeeeeee),
                        //       blurRadius: 10,
                        //       offset: Offset(0, 4),
                        //     ),
                        //   ],
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18),
                            child: Text(
                              "${selectedDate.toLocal()}".split(' ')[0],
                              // style: TextStyle(fontSize: 15),
                            ),
                          ),
                          const SizedBox(
                            height: 20.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 18),
                            child: TextButton(
                              onPressed: () => _selectDate(context),
                              child: Text(
                                'Select date',
                                style: TextStyle(color: HexColor('#F46524')),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      width: double.infinity,
                      height: 51,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await addUser(selectedDate.year, selectedDate.month,
                                int.parse(valueTextCon.text));
                          }
                        },
                        child: const Text('Save',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.black),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                HexColor('#F46524')),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.9),

                              // side: BorderSide(color: Colors.red)
                            ))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
