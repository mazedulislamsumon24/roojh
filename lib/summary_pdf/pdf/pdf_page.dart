import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';

import 'package:printing/printing.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:roojh/summary_pdf/util/util.dart';

class PdfPage extends StatefulWidget {
  const PdfPage({Key? key}) : super(key: key);

  @override
  State<PdfPage> createState() => _PdfPageState();
}

class _PdfPageState extends State<PdfPage> {
  PrintingInfo? printingInfo;
  @override
  void initState() {
    _init();
    // TODO: implement initState
    super.initState();
  }

  _init() async {
    final info = await Printing.info();
    setState(() {
      printingInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    pw.RichText.debug = true;
    final action = <PdfPreviewAction>[
      if (!kIsWeb)
        const PdfPreviewAction(
          icon: Icon(Icons.save),
          onPressed: saveAsFile,
        )
    ];
    return Scaffold(
        appBar: AppBar(
          foregroundColor: Colors.black,
          backgroundColor: Colors.white,
          title: Text(
            'Download',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
        ),
        body: PdfPreview(
            canDebug: false,
            canChangePageFormat: false,
            dynamicLayout: true,
            useActions: false,
            allowPrinting: false,
            allowSharing: false,
            maxPageWidth: double.infinity,

            // canChangeOrientation: true,
            actions: action,
            // onPrinted: showPrintedToast,
            // onShared: showsharedToast,
            build: generatedPdf));
  }
}
