import 'package:flutter/material.dart';
import 'package:roojh/homepage/Vitals/blood_pressure_chart.dart';
import './blood_sugar_chart.dart';

class Vitals extends StatefulWidget {
  const Vitals({Key? key}) : super(key: key);

  @override
  State<Vitals> createState() => _VitalsState();
}

class _VitalsState extends State<Vitals> {
  List<Widget> allVitals = [
    BloodSugarChart(),
    Padding(
      padding: const EdgeInsets.only(left: 12.0),
      child: BloodPressureChart(),
    )
  ];
  @override
  Widget build(BuildContext context) {
    return ListView(
        // shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: allVitals.map((el) => (FittedBox(child: el))).toList());
  }
}
