// ignore_for_file: deprecated_member_use
// Import the generated file

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:flutter/material.dart';
import 'package:roojh/FirebaseAuth/cofirmation_code/email_confirmation.dart';

import 'package:roojh/Sign_up/main_sign_up.dart';

import 'package:roojh/pin_password/bio_authpage.dart';
import 'package:roojh/pin_password/createpin.dart';

import 'Login_page/main_login.dart';
import 'ReportsFrom/bpForm/bpform.dart';
import 'ReportsFrom/commonForm/commonCardForms.dart';
import 'ReportsFrom/sugar_from/sugarForm.dart';
import 'forget_password/main_forgetpassword.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseFirestore.instance.settings = const Settings(
      cacheSizeBytes: Settings.CACHE_SIZE_UNLIMITED, persistenceEnabled: true);

  runApp(StartPoint());
}

class StartPoint extends StatefulWidget {
  const StartPoint({Key? key}) : super(key: key);

  @override
  State<StartPoint> createState() => _StartPointState();
}

class _StartPointState extends State<StartPoint> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Roojh',
        home: Scaffold(
            body: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState) {
              // ##############
              //check internet collection
              return Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(
                child: (Text('something went wrong')),
              );
            } else if (snapshot.hasData) {
              bool _confirm = snapshot.data!.emailVerified;
              if (_confirm) {
                return AuthPage();
              }
              // ################################################
              // if user is sign in it will redirect to Biometric Authentication
              return VerifyEmail();
            } else {
              // #########################################
              //if user is not login it will redirect to Sign In page
              return SignIn(notify: '0');
            }
          },
        )),
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
            scaffoldBackgroundColor: Color.fromARGB(255, 255, 249, 249)),
        routes: {
          "/login": (context) => SignIn(notify: '0'),
          "/forgetPass": (context) => const ForgetPassword(),
          "/signup": (context) => const SignUp(),
          "/auth": (context) => const AuthPage(),
          "/createpin": (context) => const CreatePin(),
          // sugar and blood pressure card and form
        });
  }
}
