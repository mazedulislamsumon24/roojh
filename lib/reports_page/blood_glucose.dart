import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';

class BloodGlucose extends StatefulWidget {
  String filterValue;
  String zoomValue;
  BloodGlucose({Key? key, required this.filterValue, required this.zoomValue})
      : super(key: key);

  @override
  State<BloodGlucose> createState() => _BloodGlucoseState();
}

class _BloodGlucoseState extends State<BloodGlucose> {
  String GlucoseTime = "ALL";
  String? filterValue;
  List<_ChartData> chartDataAll = [];
  List<_ChartData> chartDataFAST = [];
  List<_ChartData> chartDataPRE = [];
  List<_ChartData> chartDataPOST = [];

  String? GlucoseTimeSwitch = 'ALL';
  List fastingData = [];
  List<_ChartData> fastingList = [];
  List postmealData = [];
  List<_ChartData> postmealList = [];
  String? zoomValue;
  List max = [];
  List<_ChartData> forEmpty = [];
  @override
  void initState() {
    filterValue = widget.filterValue;
    zoomValue = widget.zoomValue;
    getDataFromFireStore(GlucoseTimeSwitch, filterValue);
    print('zoom value $zoomValue');

    super.initState();
  }

// SfCartesianChart maximum value change if highest value is greater than 190
  maxData(chartDatasMax, max) {
    max.clear();
    for (var a in chartDatasMax) {
      max.add(a.bg!.toDouble());
    }
    max.sort();
  }

  //
  User? user = FirebaseAuth.instance.currentUser;
  // List<_ChartData>? chartData = <_ChartData>[];
  DateTime todaysDate = DateTime.now();

  Map<String, double> zoomValue_map = {'100%': 0, '150%': 100, '200%': 200};
  Map<String, double> x_axis_interval = {
    'D': 0.1,
    'W': 0.05,
    'M': 0.08,
    '6M': 0.1,
    'Y': 0.1
  };
  Map<String, int> days_less = {'D': 1, 'W': 7, 'M': 31, '6M': 180, 'Y': 365};
  Map<String, dynamic> date_format = {
    'D': DateFormat.jm(),
    'W': DateFormat.E(),
    'M': DateFormat.MMMd(),
    '6M': DateFormat.MMM(),
    'Y': DateFormat.MMM()
  };

  List testTime = ['PRE LUNCH', 'POST LUNCH', 'FASTING', 'ALL'];
  Future<void> getDataFromFireStore(GlucoseTimeSwitch, fliterValue) async {
    // #################################################################
    // for switch data by date
    DateTime todaysDate_min =
        DateTime.now().subtract(Duration(days: days_less[filterValue] ?? 0));

    Timestamp timestamp = Timestamp.fromDate(todaysDate_min);

    await allDatas(timestamp);

    // calling allDatas function in order to get high and low value
    var whichTime = testTime.where((element) => element == GlucoseTimeSwitch);
    print('----======----- ${whichTime.first.toString()}');
    var snapShotsValue = await FirebaseFirestore.instance
        .collection("users")
        .doc('${user!.email}')
        .collection('vitals')
        .doc('blood_glucose')
        .collection('blood_glucose')
        .where('date', isGreaterThanOrEqualTo: timestamp)
        .where('g_time',
            isEqualTo: whichTime.first.toString() != 'ALL'
                ? whichTime.first.toString()
                : null)
        .orderBy('date', descending: false)
        .get();
    List<_ChartData> list = snapShotsValue.docs
        .map((e) => _ChartData(
            date: e.data()['date'],
            bg: e.data()['bg'],
            g_time: e.data()['g_time']))
        .toList();
    print('List $list');
    setState(() {
      chartDataAll = list;
      chartDataFAST = whichTime.first.toString() == 'FASTING' ? list : forEmpty;

      chartDataPRE =
          whichTime.first.toString() == 'PRE LUNCH' ? list : forEmpty;
      chartDataPOST =
          whichTime.first.toString() == 'POST LUNCH' ? list : forEmpty;
      maxData(list, max);
    });
    list.cast();
  }

  // #################################################################
  // all datas for lowest and highest value
  allDatas(timestamp) async {
    // for display fasting time highest and lowest sugar
    var snapShotsValue1 = await FirebaseFirestore.instance
        .collection("users")
        .doc('${user!.email}')
        .collection('vitals')
        .doc('blood_glucose')
        .collection('blood_glucose')
        .where('date', isGreaterThanOrEqualTo: timestamp)
        .where('g_time', isEqualTo: 'FASTING')
        .orderBy('date', descending: false)
        .get();
    List<_ChartData> listF = snapShotsValue1.docs
        .map((e) => _ChartData(
            date: e.data()['date'],
            bg: e.data()['bg'],
            g_time: e.data()['g_time']))
        .toList();
    print('listF $listF');
    setState(() {
      // fastingList = listF;
      fastingData.clear();
      for (var i in listF) {
        var datas = i.bg;
        fastingData.add(datas);

        print('fastig data $fastingData');
      }
      fastingData.sort();
    });

    // for display POST MEAL time highest and lowest sugar
    var snapShotsValue2 = await FirebaseFirestore.instance
        .collection("users")
        .doc('${user!.email}')
        .collection('vitals')
        .doc('blood_glucose')
        .collection('blood_glucose')
        .where('date', isGreaterThanOrEqualTo: timestamp)
        .where('g_time', isEqualTo: 'POST LUNCH')
        .orderBy('date', descending: false)
        .get();
    List<_ChartData> listP = snapShotsValue2.docs
        .map((e) => _ChartData(
            date: e.data()['date'],
            bg: e.data()['bg'],
            g_time: e.data()['g_time']))
        .toList();
    setState(() {
      postmealData.clear();
      // postmealList = listP;
      for (var i in listP) {
        var datas = i.bg;
        postmealData.add(datas);
      }
      postmealData.sort();
    });
  }

  //
  getDataSource(GlucoseTime) {
    if (GlucoseTime == "ALL") {
      return chartDataAll;
    }
    if (GlucoseTime == "FASTING") {
      return chartDataFAST;
    }
    if (GlucoseTime == "PRE LUNCH") {
      return chartDataPRE;
    }
    if (GlucoseTime == "POST LUNCH") {
      return chartDataPOST;
    } else {
      return chartDataAll;
    }
  }

  _buildLineChart() {
    return Container(
      // height: 220 + (zoomValue_map[zoomValue] ?? 0),
      child: getDataSource(GlucoseTime).length == 0
          ? const Center(child: Text(""))
          : SfCartesianChart(
              key: ObjectKey(GlucoseTime),
              primaryXAxis: DateTimeAxis(
                majorGridLines: const MajorGridLines(width: 0.6),
                // axisLine: AxisLine(width: 0),
                visibleMinimum: todaysDate
                    .subtract(Duration(days: days_less[filterValue] ?? 0))
                    .add(Duration(hours: days_less[filterValue] ?? 0)),
                //visibleMinimum: todaysDate.subtract(Duration(hours: 17)) ,
                visibleMaximum: todaysDate
                    .add(Duration(hours: (days_less[filterValue] ?? 0)) ~/ 2),
                dateFormat: date_format[filterValue],
                // isVisible: true,
                edgeLabelPlacement: EdgeLabelPlacement.shift,
                interval: x_axis_interval[filterValue],
              ),
              primaryYAxis: NumericAxis(
                // majorGridLines:
                //     MajorGridLines(width: 0.5, color: Colors.black),
                // minorGridLines:
                //     MinorGridLines(width: 0.5, color: Colors.black),
                // majorTickLines: MajorTickLines(
                //   color: Colors.black,
                //   width: 0.5,
                // ),
                // minorTickLines: MinorTickLines(
                //   color: Colors.black,
                //   width: 0.5,
                // ),
                axisLine: const AxisLine(
                  width: 0,
                ),
                minimum: 60,
                maximum: max.last >= 190 ? max.last + 30 : 200,
                interval: 60,
                labelStyle: TextStyle(
                  fontSize: 10,
                ),
              ),
              series: <ChartSeries>[
                ScatterSeries<_ChartData, Timestamp>(
                    name: 'Blood Glucose',
                    // enableTooltip: true,
                    dataSource: getDataSource(GlucoseTime),
                    xValueMapper: (_ChartData data, _) => data.date,
                    yValueMapper: (_ChartData data, _) => data.bg,
                    pointColorMapper: (_ChartData data, _) =>
                        data.bg! <= 120 ? Colors.green : Colors.red,
                    markerSettings: MarkerSettings(
                      height: 9.5 + (zoomValue_map[zoomValue] ?? 0) / 50,
                      width: 9.5 + (zoomValue_map[zoomValue] ?? 0) / 50,
                      // shape: DataMarkerType.diamond,
                    )),
              ],
              tooltipBehavior:
                  TooltipBehavior(enable: true, format: 'point.y '),
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.6),
            spreadRadius: 8,
            blurRadius: 6,
            offset: Offset(5, 5),
          ),
        ],
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          Row(
            children: const [
              Icon(Icons.bloodtype, color: Colors.red),
              Text("Blood Glucose"),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                width: 30,
              ),
              Column(
                children: [
                  Row(
                    children: const [
                      Icon(
                        Icons.circle,
                        color: Colors.red,
                        size: 10,
                      ),
                      Text(" FASTING", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                  Container(
                    child: fastingData.length > 1
                        ? Text(
                            "${fastingData.first} - ${fastingData.last}",
                            style: TextStyle(fontSize: 14),
                          )
                        : Text(
                            " __ - __",
                            style: TextStyle(fontSize: 14),
                          ),
                  )
                ],
              ),
              const SizedBox(
                width: 50,
              ),
              Column(
                children: [
                  Row(
                    children: const [
                      Icon(
                        Icons.circle,
                        color: Colors.green,
                        size: 10,
                      ),
                      Text(" POST MEAL", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                  Container(
                    child: postmealData.length >= 2
                        ? Text(
                            "${postmealData.first} - ${postmealData.last}",
                            style: TextStyle(fontSize: 14),
                          )
                        : Text(
                            " __ - __",
                            style: TextStyle(fontSize: 14),
                          ),
                  )
                ],
              ),
              const SizedBox(
                width: 2,
              ),
              Column(
                children: const [
                  Text(""),
                  Padding(
                    padding: EdgeInsets.only(top: 1.0),
                    child: Text("", style: TextStyle(fontSize: 14)),
                  )
                ],
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          FittedBox(
              child: SizedBox(
            child: _buildLineChart(),
            height: 150 + (zoomValue_map[zoomValue] ?? 0),
            width: 400,
          )),
          FittedBox(
            child: CustomRadioButton(
              buttonTextStyle: const ButtonTextStyle(
                selectedColor: Colors.white,
                unSelectedColor: Colors.black,
                textStyle: TextStyle(
                  fontSize: 14,
                ),
              ),
              buttonLables: const [
                "ALL",
                "FASTING",
                "PRE LUNCH",
                "POST LUNCH",
              ],
              buttonValues: const [
                "ALL",
                "FASTING",
                "PRE LUNCH",
                "POST LUNCH",
              ],
              spacing: 0,
              defaultSelected: "ALL",
              horizontal: false,
              enableButtonWrap: false,
              width: 75,
              absoluteZeroSpacing: false,
              selectedColor: Color(0xFFF46524),
              padding: 1,
              unSelectedColor: Colors.white,
              enableShape: true,
              autoWidth: true,
              radioButtonValue: (value) {
                setState(() {
                  GlucoseTime = value.toString();
                  String? GlucoseTimeSwitch = GlucoseTime;
                  getDataFromFireStore(GlucoseTimeSwitch, filterValue);
                });
                print(GlucoseTime);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _ChartData {
  _ChartData({this.bg, this.date, this.g_time});
  final num? bg;
  final Timestamp? date;
  final String? g_time;
}
