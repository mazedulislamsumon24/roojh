import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../FirebaseAuth/firebase_storage/firebase_storage.dart';
import '../select_report.dart';

class KidneyForm extends StatefulWidget {
  const KidneyForm({Key? key}) : super(key: key);

  @override
  State<KidneyForm> createState() => _KidneyFormState();
}

class _KidneyFormState extends State<KidneyForm> {
  final _formKey = GlobalKey<FormState>();
  final gLevel = TextEditingController();
  String? user = FirebaseAuth.instance.currentUser!.email;
  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2020, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  CollectionReference users =
      FirebaseFirestore.instance.collection('usersKidneyData');

  Future<void> addUser(int date, int month, int gLevel) {
    return users
        .doc('${user}')
        .collection('KidneyData')
        .doc('$date')
        .set({'date': date, 'gLevel': gLevel}).then((value) {
      print("User Added");
    }).catchError((error) => print("Failed to add user: $error"));
  }

  final auth = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: AppBar(
        actions: [],
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => SelectReportForm()));
          }, // Handle your on tap here.
          icon: SvgPicture.asset('icons/arrow - right.svg'),
        ),
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Center(
          child: Text(
            "Kidney Function",
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Container(
                      child: Column(children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Kidney Function',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.red.shade600),
                            )),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          // obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                            hintText: "120",
                            //create lable
                            labelText: 'mmHg',
                            //lable style
                            labelStyle: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                              fontFamily: "verdana_regular",
                              fontWeight: FontWeight.w400,
                            ),
                            filled: true,
                            fillColor: HexColor('#F3F6FF'),
                            // border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.blue,
                                width: 2.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 2.0,
                              ),
                            ),
                            // hintText: "120",
                          ),
                          controller: gLevel,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'please enter gLeveltolic Value';
                            }
                          },
                        )
                      ]),
                      padding: EdgeInsets.all(22),
                      height: 165,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: HexColor('#F3F6FF'),
                          borderRadius: BorderRadius.all(Radius.circular(23)),
                          boxShadow: [
                            // BoxShadow(blurRadius: 1.0),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: Offset(0, -5)),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: (Offset(5, 0)))
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      height: 51,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: HexColor('#F3F6FF'),
                        borderRadius: BorderRadius.circular(98),
                        border: Border.all(color: HexColor('#CED3E1')),
                        //   boxShadow: [
                        //     BoxShadow(
                        //       color: Color(0xffeeeeee),
                        //       blurRadius: 10,
                        //       offset: Offset(0, 4),
                        //     ),
                        //   ],
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18),
                            child: Text(
                              "${selectedDate.toLocal()}".split(' ')[0],
                              // style: TextStyle(fontSize: 15),
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 18),
                            child: TextButton(
                              onPressed: () => _selectDate(context),
                              child: Text('Select date'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      width: double.infinity,
                      height: 51,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await addUser(selectedDate.year, selectedDate.month,
                                int.parse(gLevel.text));
                          }
                        },
                        child: const Text('Save',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.black),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                HexColor('#F46524')),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.9),

                              // side: BorderSide(color: Colors.red)
                            ))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
