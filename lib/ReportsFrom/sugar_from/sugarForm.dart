import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/common_code/footer.dart';

import '../../reports_page/reports_page.dart';

class SugarForm extends StatefulWidget {
  const SugarForm({Key? key}) : super(key: key);

  @override
  State<SugarForm> createState() => _SugarFormState();
}

class _SugarFormState extends State<SugarForm> {
  final _formKey = GlobalKey<FormState>();
  final bg = TextEditingController();

  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2020, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

// for update tiles after saving value in bloodglucoss
  Future<void> updateTiles(int bg) async {
    FirebaseFirestore.instance
        .collection('users')
        .doc('${auth!.email}')
        .collection('tiles')
        .doc('blood_glucose')
        .update({'value': bg.toString()});
  }

  final auth = FirebaseAuth.instance.currentUser;

  Future<void> addUser(date, int bg, radiobutton) async {
    return FirebaseFirestore.instance
        .collection('users')
        .doc('${auth!.email}')
        .collection('vitals')
        .doc('blood_glucose')
        .collection('blood_glucose')
        .add({'date': selectedDate, 'g_time': radiobutton, 'bg': bg}).then(
            (value) async {
      await updateTiles(bg);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Colors.green,
          duration: Duration(seconds: 5),
          content: Text('Data Submited Successfully'),
        ),
      );
      // await Future.delayed(const Duration(seconds: 2));
      await Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => Footer(
                landingIndex: 3, // 3 denotes to show reports page
              )));
    }).catchError((error) => ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('Something Went Wrong'),
              ),
            ));
  }

  String radioButtonItem = 'FASTING';
  Color radio_color = HexColor('#204289');

  // Group Value for Radio Button.
  int id = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            if (Navigator.canPop(context)) {
              Navigator.pop(context);
            } else {
              SystemNavigator.pop();
            }
          }, // Handle your on tap here.
          icon: SvgPicture.asset('icons/arrow - right.svg'),
        ),
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyRadiob object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          "Blood Glucose",
          style: TextStyle(
            color: Colors.black.withOpacity(0.8),
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Container(
                      child: Column(children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Blood Glucose',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.red.shade600),
                            )),
                        SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 100,
                              child: TextFormField(
                                // obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w600),
                                decoration: InputDecoration(
                                  hintText: "120",
                                  //create lable
                                  hintStyle: TextStyle(
                                      color: Colors.black.withOpacity(0.2)),
                                  filled: true,
                                  fillColor: HexColor('#F3F6FF'),
                                  // border: InputBorder.none,
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.0)),
                                  ),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(30.0, 15.0, 20.0, 0),

                                  // hintText: "120",
                                ),
                                controller: bg,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'please enter Systolic Value';
                                  }
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 32, left: 10),
                              child: Text(
                                'mg/dL',
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.3)),
                              ),
                            )
                          ],
                        )
                      ]),
                      padding: EdgeInsets.all(22),
                      height: 165,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: HexColor('#F3F6FF'),
                          borderRadius: BorderRadius.all(Radius.circular(23)),
                          boxShadow: [
                            // BoxShadow(blurRadius: 1.0),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: Offset(0, -5)),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: (Offset(5, 0)))
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  // ################################################
                  // RADIO BUTTONS______________________________________________________
                  Padding(
                    padding: EdgeInsets.only(
                        top: 15, bottom: 15, left: 20, right: 20),
                    child: Container(
                      decoration: BoxDecoration(
                          color: HexColor('#F3F6FF'),
                          borderRadius: BorderRadius.all(Radius.circular(23)),
                          boxShadow: [
                            // BoxShadow(blurRadius: 1.0),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: Offset(0, -5)),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: (Offset(5, 0)))
                          ]),
                      height: 190,
                      width: 320,
// ################################################
                      // Start Stack
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Positioned(
                            top: 18,
                            left: 28,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 1,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'FASTING';

                                      id = 1;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'FASTING',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 18,
                            left: 120,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 2,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'POST BREAKFAST';
                                      id = 2;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'POST BREAKFAST',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 18,
                            left: 245,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 3,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'PRE LUNCH';
                                      id = 3;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'PRE LUNCH',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 80,
                            left: 21,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 4,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'PRE LUNCH';
                                      id = 4;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'POST LUNCH',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 80,
                            left: 131,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 5,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'PRE DINNER';
                                      id = 5;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'PRE DINNER',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 80,
                            left: 240,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 6,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'POST DINNER';
                                      id = 6;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'POST DINNER',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 140,
                            left: 33,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 7,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = '3 AM';
                                      id = 7;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '3 AM',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 140,
                            left: 137,
                            child: Column(
                              children: [
                                Radio(
                                  visualDensity: const VisualDensity(
                                      horizontal: VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: HexColor('#204289'),
                                  focusColor: HexColor('#204289'),
                                  value: 8,
                                  groupValue: id,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButtonItem = 'RANDOM';
                                      id = 8;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'RANDOM',
                                  style: new TextStyle(
                                      fontSize: 9,
                                      color: Colors.black.withOpacity(0.6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                              top: 10,
                              left: 90,
                              child: Container(
                                width: 2.5,
                                height: 170,
                                decoration: BoxDecoration(
                                  //   gradient: LinearGradient(
                                  //     begin: Alignment.topCenter,
                                  //     end: Alignment.bottomCenter,
                                  //     colors: [HexColor('#F3F6FF'), Colors.black],
                                  //   ),
                                  color: Colors.black.withOpacity(0.3),
                                ),
                              )),
                          Positioned(
                              top: 10,
                              left: 220,
                              child: Container(
                                width: 2.5,
                                height: 170,
                                decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.3)),
                              )),
                          // let to right
                          Positioned(
                              left: 10,
                              top: 65,
                              child: Container(
                                width: 300,
                                height: 2.5,
                                decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.3)),
                              )),
                          Positioned(
                              left: 10,
                              top: 128,
                              child: Container(
                                width: 300,
                                height: 2.5,
                                decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.3)),
                              )),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      height: 51,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: HexColor('#F3F6FF'),
                        borderRadius: BorderRadius.circular(98),
                        border: Border.all(color: HexColor('#CED3E1')),
                        //   boxShadow: [
                        //     BoxShadow(
                        //       color: Color(0xffeeeeee),
                        //       blurRadius: 10,
                        //       offset: Offset(0, 4),
                        //     ),
                        //   ],
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18),
                            child: Text(
                              "${selectedDate.toLocal()}".split(' ')[0],
                              // style: TextStyle(fontSize: 15),
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 18),
                            child: TextButton(
                              onPressed: () => _selectDate(context),
                              child: Text(
                                'Select Date',
                                style: TextStyle(
                                    color: HexColor('#F46524').withOpacity(0.9),
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      width: double.infinity,
                      height: 51,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await addUser(selectedDate, int.parse(bg.text),
                                radioButtonItem);
                          }
                        },
                        child: const Text('Save',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.black),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                HexColor('#F46524')),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.9),

                              // side: BorderSide(color: Colors.red)
                            ))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
