import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:hexcolor/hexcolor.dart';

import 'package:roojh/showFiles/pdfView.dart';
import 'package:roojh/showFiles/roojh_summary/summary.dart';
import 'package:super_tooltip/super_tooltip.dart';

import '../ReportsFrom/select_report.dart';

class DocsDeatils {
  Timestamp date;
  String documentType;
  String email;
  String imgUrl;
  String month;
  String pdfUrl;
  String summary;
  String uid;
  String status;
  String description;

  DocsDeatils(
      {required this.date,
      required this.documentType,
      required this.email,
      required this.imgUrl,
      required this.month,
      required this.pdfUrl,
      required this.summary,
      required this.uid,
      required this.description,
      required this.status});
}

// this class will show your uploaded documents and it called in footer class 'common_code/footer.dart'
class MainShowFile extends StatefulWidget {
  const MainShowFile({Key? key}) : super(key: key);

  @override
  State<MainShowFile> createState() => _MainShowFileState();
}

class _MainShowFileState extends State<MainShowFile> {
  String? userEmail = FirebaseAuth.instance.currentUser!.email;
  User? user = FirebaseAuth.instance.currentUser;

  var year = '2022';
  var groups;

  // for first second third in day
  static final _dayMap = {1: 'st', 2: 'nd', 3: 'rd'};
  static String dayOfMonth(int day) => "$day${_dayMap[day] ?? 'th'}";

  // List docsList = [];
  List listDatas = [];
  bool status = false;

  Future<void> getdata(year) async {
    // ##################################
    //get data from db in list allAvailableCard
    var snapShotsValue1 = await FirebaseFirestore.instance
        .collection('users')
        .doc(userEmail)
        .collection('documents')
        .where('year', isEqualTo: year)
        .orderBy('date', descending: true)
        // .where('month', isEqualTo: months2.asMap())
        .get();
    List<DocsDeatils> listF = snapShotsValue1.docs
        .map((e) => DocsDeatils(
              date: e.data()['date'],
              documentType: e.data()['documentType'],
              imgUrl: e.data()['imgUrl'],
              pdfUrl: e.data()['pdfUrl'],
              summary: e.data()['summary'],
              month: e.data()['month'],
              uid: e.data()['uid'],
              description: e.data()['description'],
              status: e.data()['status'],
              email: e.data()['email'],
            ))
        .toList();

    listDatas = listF;

// add data form listDatas in allAvailableCards
    byMonth(listDatas); //send list to get in month wise data
    setState(() {
      if (groups != null) {
        status = true;
      }
    });
  }

// function in order to get month  wise data
  void byMonth(listDatas) {
    groups = groupBy(listDatas, (DocsDeatils e) {
      return e.month;
    });

    print(groups);
  }

  @override
  void initState() {
    getdata(year); //calling main function

    // TODO: implement initState
    super.initState();
  }
//    final keyRefresh = GlobalKey<RefreshIndicatorState>();
//      int page = 0;
//   ScrollController _scrollController = new ScrollController();
//   bool isLoading = false;

// Future getData() async {
//     keyRefresh.currentState?.show();
//     if (!isLoading) {
//       setState(() {
//         page++;
//         isLoading = true;
//       });

//       WpApi.getPostsList(category: widget.category, page: page).then((_posts) {
//         setState(() {
//           isLoading = false;
//           posts = _posts
//         });
//       });
//     }
//   }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(),
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundImage: Image.network("${user!.photoURL}").image,
            ),
          ),
        ],
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Center(
          child: Text(
            "Documents",
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
      body: RefreshIndicator(
        strokeWidth: 2,
        color: Colors.white,
        backgroundColor: Colors.blue.shade900,
        triggerMode: RefreshIndicatorTriggerMode.onEdge,
        onRefresh: () async {
          // await Future.delayed(Duration(seconds: 2));
          await getdata(year);
        },
        child: ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 22.5, right: 22.5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 183,
                        height: 34,
                        child: TextField(
                            style: const TextStyle(
                              fontSize: 13.0,
                              color: Colors.blueAccent,
                            ),
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.fromLTRB(
                                    0.0, 8.0, 0, 15.0),
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: HexColor('#204289'),
                                ),
                                hintText: "Search",
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: HexColor('#F4F5F9'),
                                        width: 32.0),
                                    borderRadius: BorderRadius.circular(17.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: HexColor('#F4F5F9'),
                                        width: 32.0),
                                    borderRadius:
                                        BorderRadius.circular(17.0)))),
                      ),
                      Container(
                          child: TextButton(
                        child: SvgPicture.asset('icons/filter.svg'),
                        onPressed: () {},
                      ))
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                CustomRadioButton(
                  enableButtonWrap: true,
                  selectedBorderColor: HexColor('#F46524'),
                  unSelectedBorderColor: HexColor('#F46524'),
                  width: 85,
                  defaultSelected: '2022',
                  elevation: 0,
                  absoluteZeroSpacing: true,
                  unSelectedColor: Theme.of(context).canvasColor,
                  buttonLables: const ['2020', '2021', '2022'],
                  buttonValues: const ['2020', '2021', '2022'],
                  buttonTextStyle: const ButtonTextStyle(
                      selectedColor: Colors.white,
                      unSelectedColor: Colors.black,
                      textStyle: TextStyle(fontSize: 16)),
                  radioButtonValue: (value) {
                    setState(() {
                      year = value as String;
                      getdata(year); //calling main function
                    });
                  },
                  selectedColor: HexColor('#F46524'),
                ),
                const SizedBox(
                  height: 15,
                ),
                Center(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: HexColor('#F46524'),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                    ),
                    child: Text('Roojh Summary'),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => Summary(
                              // 3 denotes to show reports page
                              )));
                    },
                  ),
                ),
                status
                    ? SingleChildScrollView(
                        child: Column(
                          children: [
                            for (MapEntry e in groups.entries)
                              Container(
                                // width: 300,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, top: 7, right: 20),
                                  child: Column(
                                    // mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            '${e.key} ${year}',
                                            style: const TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const Flexible(
                                            child: Divider(
                                              height: 0.2,
                                              thickness: 0.35,
                                              indent: 18,
                                              endIndent: 0,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ],
                                      ),
                                      // SizedBox(
                                      //   height: 5,
                                      // ),

                                      for (var i in e.value)
                                        InkWell(
                                            onTap: () {},
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 7,
                                                    top: 5,
                                                  ),
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    // mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      Container(
                                                        width: 27,
                                                        height: 27,
                                                        decoration:
                                                            BoxDecoration(
                                                          boxShadow: const [
                                                            BoxShadow(
                                                                color:
                                                                    Colors.grey,
                                                                blurRadius: 1,
                                                                spreadRadius: 1,
                                                                offset: Offset(
                                                                    0, 0)),
                                                            BoxShadow(
                                                                color:
                                                                    Colors.grey,
                                                                blurRadius: 1,
                                                                spreadRadius: 1,
                                                                offset: Offset(
                                                                    0, 0)),
                                                          ],
                                                          color: const Color(
                                                                  0xFFF46524)
                                                              .withOpacity(
                                                                  0.83), // border color
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        child: Padding(
                                                          padding: const EdgeInsets
                                                                  .all(
                                                              2), // border width
                                                          child: Container(
                                                            // or ClipRRect if you need to clip the content
                                                            decoration:
                                                                const BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color: Color.fromARGB(
                                                                  255,
                                                                  255,
                                                                  249,
                                                                  249), // inner circle color
                                                            ),
                                                            child: Center(
                                                                child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  dayOfMonth((i
                                                                              .date
                                                                          as Timestamp)
                                                                      .toDate()
                                                                      .day),
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      fontSize:
                                                                          8,
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              0.8)),
                                                                )
                                                              ],
                                                            )), // inner content
                                                          ),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                          // height: 1,
                                                          ),
                                                      // SvgPicture.asset(
                                                      //     'icons/EllipseRound.svg'),
                                                      Container(
                                                        // ##################
                                                        // size of vertical divider
                                                        height: 35,
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                bottom: 5,
                                                                left: 10,
                                                                right: 10),
                                                        child:
                                                            const VerticalDivider(
                                                          color: Colors.black,
                                                          thickness: 0.35,
                                                          indent: 0,
                                                          endIndent: 0,
                                                          width: 0.5,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                // #$#$$$$$$$$$$$$$$$$#$#$
                                                // box size
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Container(
                                                    width: 280,
                                                    height: 39,
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 7,
                                                            bottom: 7,
                                                            left: 10,
                                                            right: 15),
                                                    decoration: BoxDecoration(
                                                      color:
                                                          HexColor('#F3F6FF'),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9),
                                                      border: Border.all(
                                                          color: HexColor(
                                                              '#CED3E1')),
                                                    ),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Row(
                                                          // mainAxisAlignment:
                                                          //     MainAxisAlignment
                                                          //         .spaceBetween,
                                                          children: [
                                                            // this tooltip is for doc status

                                                            SizedBox(
                                                              width: 55,
                                                              child: Text(
                                                                i.documentType
                                                                            .length <
                                                                        8
                                                                    ? "${i.documentType}   "
                                                                    : "${i.documentType.toString().substring(0, 8)}..",
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),
                                                            ),
                                                            const SizedBox(
                                                              width: 15,
                                                            ),
                                                            i.status ==
                                                                    "completed"
                                                                ? Center(
                                                                    child: Text(
                                                                      i.description.length <=
                                                                              21
                                                                          ? "${i.description}"
                                                                          : "${i.description.toString().substring(0, 19)}..",
                                                                      style: TextStyle(
                                                                          color: Colors.black.withOpacity(
                                                                              0.55),
                                                                          fontSize:
                                                                              12,
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                    ),
                                                                  )
                                                                : Container(
                                                                    height: 20,
                                                                    width: 89,
                                                                    decoration: BoxDecoration(
                                                                        // color: i.status == 'New'
                                                                        //     ? Colors.blue.shade400
                                                                        //     : i.status == 'In Progress'
                                                                        //         ? Colors.grey
                                                                        //         : i.status == 'Rejected'
                                                                        //             ? Colors.red.shade300
                                                                        //             : Colors.green,
                                                                        color: i.status == 'new'
                                                                            ? Colors.green.shade300
                                                                            : i.status == 'inProgress'
                                                                                ? Colors.blue.shade300
                                                                                : i.status == 'rejected'
                                                                                    ? Colors.red.shade200
                                                                                    : null,
                                                                        borderRadius: BorderRadius.all(Radius.circular(22))),
                                                                    child:
                                                                        Center(
                                                                      child:
                                                                          Text(
                                                                        i.status ==
                                                                                'new'
                                                                            ? "New"
                                                                            : i.status == 'inProgress'
                                                                                ? 'In Progress'
                                                                                : i.status == 'rejected'
                                                                                    ? "Rejected"
                                                                                    : "",
                                                                        style: TextStyle(
                                                                            color: Colors
                                                                                .white,
                                                                            fontSize:
                                                                                12,
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                      ),
                                                                    ),
                                                                  ),
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Tooltip(
                                                              //padding under tooltip
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 15,
                                                                      bottom:
                                                                          15,
                                                                      left: 10,
                                                                      right: 5),
                                                              //decoration color and shadow
                                                              decoration:
                                                                  const BoxDecoration(
                                                                      boxShadow: [
                                                                    // BoxShadow(
                                                                    //   color:
                                                                    //       Colors.grey,
                                                                    //   offset:
                                                                    //       Offset(
                                                                    //     3.0,
                                                                    //     3.0,
                                                                    //   ),
                                                                    //   blurRadius:
                                                                    //       5.0,
                                                                    //   spreadRadius:
                                                                    //       1.0,

                                                                    // ),
                                                                    BoxShadow(
                                                                        color: Colors
                                                                            .grey,
                                                                        blurRadius:
                                                                            3,
                                                                        spreadRadius:
                                                                            5,
                                                                        offset: Offset(
                                                                            0,
                                                                            0)),
                                                                    BoxShadow(
                                                                        color: Colors
                                                                            .grey,
                                                                        blurRadius:
                                                                            5,
                                                                        spreadRadius:
                                                                            1,
                                                                        offset: Offset(
                                                                            5,
                                                                            5)),
                                                                  ],
                                                                      color: Colors
                                                                          .white),
                                                              // for text color
                                                              textStyle:
                                                                  const TextStyle(
                                                                      color: Colors
                                                                          .black),
                                                              height: 200,
                                                              // verticalOffset:
                                                              //     50,
                                                              margin:
                                                                  const EdgeInsets
                                                                      .only(
                                                                left: 20,
                                                                right: 20,
                                                              ),
                                                              triggerMode:
                                                                  TooltipTriggerMode
                                                                      .tap,

                                                              message:
                                                                  i.summary,

                                                              child: Container(
                                                                width: 22,
                                                                height: 22,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: Colors
                                                                      .green
                                                                      .withOpacity(
                                                                          0.25), // border color
                                                                  shape: BoxShape
                                                                      .circle,
                                                                ),
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .all(
                                                                      2), // border width
                                                                  child:
                                                                      Container(
                                                                    // or ClipRRect if you need to clip the content
                                                                    decoration:
                                                                        const BoxDecoration(
                                                                      shape: BoxShape
                                                                          .circle,
                                                                      color: Color(
                                                                          0xFFF46524), // inner circle color
                                                                    ),
                                                                    child: const Center(
                                                                        child: Text(
                                                                      "i",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.white),
                                                                    )), // inner content
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 15,
                                                            ),
                                                            SizedBox(
                                                              child: i.pdfUrl
                                                                          .toString() !=
                                                                      ""
                                                                  ? InkWell(
                                                                      onTap:
                                                                          () {
                                                                        Navigator.of(context).push(MaterialPageRoute(
                                                                            builder: (context) =>
                                                                                PdfView(datas: i)));
                                                                      },
                                                                      child: Image
                                                                          .asset(
                                                                        'icons/1-removebg-preview.png',
                                                                        height:
                                                                            24,
                                                                        width:
                                                                            24,
                                                                      ))
                                                                  : InkWell(
                                                                      onTap:
                                                                          () {
                                                                        Navigator.of(context).push(MaterialPageRoute(
                                                                            builder: (context) =>
                                                                                PdfView(datas: i)));
                                                                      },
                                                                      child: Image
                                                                          .asset(
                                                                        'icons/photo.png',
                                                                        height:
                                                                            22,
                                                                        width:
                                                                            22.5,
                                                                      )),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    ))
                                              ],
                                            )),
                                    ],
                                  ),
                                ),
                                // leading: Image.network(data[index]["body"]),
                              ),
                          ],
                        ),
                      )
                    : const Center(child: CircularProgressIndicator())
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// original


















