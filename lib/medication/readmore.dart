import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/medication/medication_form/editForm.dart';

import '../common_code/footer.dart';

class MedReadMore extends StatefulWidget {
  QueryDocumentSnapshot<Object?> usersdata;
  MedReadMore({Key? key, required this.usersdata}) : super(key: key);

  @override
  State<MedReadMore> createState() => _MedReadMoreState();
}

class _MedReadMoreState extends State<MedReadMore> {
  User? user = FirebaseAuth.instance.currentUser;
  QueryDocumentSnapshot<Object?>? usersdata;
  List<dynamic>? list1;
  @override
  void initState() {
    usersdata = widget.usersdata;

    // TODO: implement initState
    super.initState();
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text('Are you sure to delete this data'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(backgroundColor: Colors.red),
              child: Text('Confirm', style: TextStyle(color: Colors.white)),
              onPressed: () async {
                print('Confirmed');
                await FirebaseFirestore.instance
                    .collection('users')
                    .doc('${user!.email}')
                    .collection('medication')
                    .doc(usersdata!.id.toString())
                    .delete()
                    .then((value) async {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Colors.red,
                      duration: Duration(seconds: 5),
                      content: Text('Data deleted Successfully'),
                    ),
                  );
                  // await Future.delayed(const Duration(seconds: 2));
                  await Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => Footer(
                            landingIndex: 2, // 3 denotes to show reports page
                          )));
                });
                ;
              },
            ),
            TextButton(
              style: TextButton.styleFrom(backgroundColor: Colors.green),
              child: Text(
                'Cancel',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,

        leading: IconButton(
          onPressed: () {
            if (Navigator.canPop(context)) {
              Navigator.pop(context);
            } else {
              SystemNavigator.pop();
            }
          }, // Handle your on tap here.
          icon: SvgPicture.asset('icons/arrow - right.svg'),
        ),
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          "Medication",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                SizedBox(
                  height: 25,
                ),
                Column(children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      usersdata!['medication_name'],
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Dose',
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 17),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "${usersdata!['dose']} ${usersdata!['type']} ",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15.75,
                          color: Colors.black.withOpacity(0.5)),
                    ),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Dose Timing',
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 17),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "${usersdata!['dose_times']}",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15.75,
                          color: Colors.black.withOpacity(0.5)),
                    ),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  // Align(
                  //   alignment: Alignment.topLeft,
                  //   child: Text(
                  //     'Description',
                  //     style:
                  //         TextStyle(fontWeight: FontWeight.w600, fontSize: 17),
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: 4,
                  // ),
                  // Align(
                  //   alignment: Alignment.topLeft,
                  //   child: Text(
                  //     usersdata!['description'],
                  //     style: TextStyle(
                  //         fontWeight: FontWeight.w500,
                  //         fontSize: 15.75,
                  //         color: Colors.black.withOpacity(0.5)),
                  //   ),
                  // ),//
                ]),
                SizedBox(
                  height: 80,
                ),
                Container(
                  child: usersdata!['admin_verified']
                      ? Column(
                          children: [
                            Icon(
                              Icons.verified_user,
                              color: Colors.green,
                              size: 20,
                            ),
                            Text(
                              'Verified',
                              style: TextStyle(fontSize: 10),
                            )
                          ],
                        )
                      : Column(
                          children: [
                            Icon(
                              Icons.dangerous_rounded,
                              color: Colors.red,
                              size: 20,
                            ),
                            Text(
                              'Not Verified',
                              style: TextStyle(fontSize: 10),
                            )
                          ],
                        ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: double.infinity,
                  height: 51,
                  child: ElevatedButton(
                    onPressed: () async {
                      await _showMyDialog();
                    },
                    child: const Text('Delete',
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.red,
                            fontWeight: FontWeight.w600)),
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        backgroundColor: MaterialStateProperty.all<Color>(
                          HexColor('#FEF0E9'),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.9),

                          // side: BorderSide(color: Colors.red)
                        ))),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: double.infinity,
                  height: 51,
                  child: ElevatedButton(
                    onPressed: () async {
                      // await Future.delayed(const Duration(seconds: 2));
                      await Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  EditMedForm(usersdata: usersdata)));
                    },
                    child: const Text('Edit',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Colors.white)),
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        backgroundColor: MaterialStateProperty.all<Color>(
                          HexColor("#F46524"),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.9),

                          // side: BorderSide(color: Colors.red)
                        ))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
