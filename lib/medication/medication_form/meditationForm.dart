// ignore_for_file: deprecated_member_use

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import 'package:hexcolor/hexcolor.dart';

import '../../common_code/footer.dart';

//import 'package:awesome_dropdown/awesome_dropdown.dart';
class DataController extends GetxController {
  Future getData(String collection) async {
    final FirebaseFirestore firebaseStore = FirebaseFirestore.instance;
    QuerySnapshot snapshot = await firebaseStore.collection(collection).get();
    return snapshot.docs;
  }

  Future queryData(String queryString) async {
    return await FirebaseFirestore.instance
        .collection('med_datas')
        .where('med_name', isGreaterThanOrEqualTo: queryString)
        .get();
  }
}

class MeditationForm extends StatefulWidget {
  const MeditationForm({
    Key? key,
  }) : super(key: key);

  @override
  State<MeditationForm> createState() => _MeditationFormState();
}

class _MeditationFormState extends State<MeditationForm> {
  String name = "";
  String? selecTest;
  String? dose;
  final doseController = TextEditingController();
  final auth = FirebaseAuth.instance.currentUser;
  var getdose;
  var doseType = 'mg';
  var frequency;
  var getVal;

  // ##############################
  //For Date form

  TimeOfDay _time = TimeOfDay.now();
  List timesList = [];
  void _selectTime() async {
    final TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: _time,
    );
    if (newTime != null) {
      setState(() {
        _time = newTime;
        timesList.add(newTime.format(context));
      });
      print(timesList.toString());
    }
  }

  late final GlobalKey<FormState> _formKey;
  @override
  void initState() {
    _formKey = GlobalKey<FormState>();

    // TODO: implement initState
    super.initState();
  }

  QuerySnapshot? snapshotData;
  var company;
  bool? isexecuted = false;
  final TextEditingController searchController = TextEditingController();
  Widget searchData() {
    return ListView.builder(
      itemCount: snapshotData!.docs.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          onTap: () {
            setState(() {
              getVal =
                  (snapshotData?.docs[index].data() as dynamic)['med_name'];
              company =
                  (snapshotData?.docs[index].data() as dynamic)['company'];
              print(getVal);
              isexecuted = false;
            });
          },
          title: Text(
            "${(snapshotData?.docs[index].data() as dynamic)['med_name']}",
            style: TextStyle(fontSize: 16),
          ),
          subtitle: Text(
            "${(snapshotData?.docs[index].data() as dynamic)['company']}",
            style: TextStyle(fontSize: 13),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final summaryController = TextEditingController();
    // final _formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("#F46524"),
        actions: [
          GetBuilder<DataController>(
            init: DataController(),
            builder: (val) {
              return IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  val.queryData(searchController.text).then((value) {
                    setState(() {
                      snapshotData = value;
                      isexecuted = true;
                    });
                  });
                },
              );
            },
          )
        ],
        title: TextField(
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            hintText: 'Search Medicine',
            hintStyle: TextStyle(color: Colors.white),
          ),
          controller: searchController,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // Padding(
            //   padding: const EdgeInsets.only(
            //       left: 16.0, right: 10, top: 10, bottom: 10),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       SizedBox(),
            //       Text(
            //         "Medication Detail",
            //         style: TextStyle(
            //           fontSize: 17,
            //           fontWeight: FontWeight.w600,
            //         ),
            //       ),
            //       Align(
            //           alignment: Alignment.bottomRight,
            //           child: TextButton(
            //             child: SvgPicture.asset('icons/close.svg'),
            //             onPressed: () {
            //               if (Navigator.canPop(context)) {
            //                 Navigator.pop(context);
            //               } else {
            //                 SystemNavigator.pop();
            //               }
            //             },
            //             style: TextButton.styleFrom(
            //               // shadowColor: Colors.white,
            //               minimumSize: Size.zero,
            //               // backgroundColor:
            //               //     const Color.fromARGB(255, 255, 249, 249),
            //               padding: EdgeInsets.zero,
            //               tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            //             ),
            //           )),
            //     ],
            //   ),
            // ),
            SizedBox(
              height: isexecuted == true ? 500 : 0,
              child: isexecuted == true
                  ? searchData()
                  : Center(
                      child: Container(),
                    ),
            ),
            isexecuted != true
                ? Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 25,
                          ),
                          Container(
                            child: Column(children: [
                              Text(
                                'Medicine',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.red.shade600),
                              ),
                              // ####################
                              // medicine name
                              Padding(
                                padding: const EdgeInsets.only(top: 3),
                                child: Text(
                                  getVal == null ? '' : getVal,
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              // ####################
                              // company name
                              Padding(
                                padding: const EdgeInsets.only(top: 3),
                                child: Text(
                                  company == null ? '' : company,
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              // #@############################
                              // Dose Card
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: 40),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Dose',
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w700,
                                              // color: Colors.red.shade600
                                              // color: Colors.black
                                              color:
                                                  Colors.black.withOpacity(0.7)
                                              // color: HexColor('#204289')
                                              ),
                                        ),
                                        Text(
                                          '(optional)',
                                          style: TextStyle(fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: 80,
                                    child: TextFormField(
                                      // obscureText: true,
                                      enableSuggestions: false,
                                      autocorrect: false,
                                      style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w600),

                                      decoration: InputDecoration(
                                        hintText: "500",

                                        //create lable

                                        filled: true,
                                        fillColor: HexColor('#F3F6FF'),
                                        // border: InputBorder.none,
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black
                                                  .withOpacity(0.0)),
                                        ),
                                        contentPadding:
                                            const EdgeInsets.fromLTRB(
                                                30.0, 15.0, 20.0, 0),

                                        // hintText: "120",
                                      ),
                                      controller: doseController,
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return 'please enter Dose Value';
                                        }
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 32,
                                    ),
                                    // ignore: sized_box_for_whitespace
                                    child: Container(
                                      width: 90,

                                      height: 54,

                                      // width: double.infinity,
                                      child: Container(
                                        child: DropdownButton<String>(
                                          underline: Container(),
                                          isExpanded: true,

                                          hint: Text('$doseType',
                                              style: TextStyle(fontSize: 15)),
                                          items: <String>[
                                            'mg',
                                            'micromg',
                                            'unit',
                                          ].map((String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(
                                                value,
                                                style: TextStyle(fontSize: 15),
                                              ),
                                            );
                                          }).toList(),
                                          onChanged: (String? value) {
                                            setState(() {
                                              doseType = value!;
                                            });
                                          },

                                          // autofocus: true,
                                          value: doseType,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ]),
                            padding:
                                EdgeInsets.only(top: 22, left: 22, right: 22),
                            height: 190,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: HexColor('#F3F6FF'),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(23)),
                                boxShadow: const [
                                  // BoxShadow(blurRadius: 1.0),
                                  BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: Offset(0, -5)),
                                  BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: (Offset(5, 0)))
                                ]),
                          ),

                          const SizedBox(
                            height: 30,
                          ),

                          Container(
                            height: 190,
                            decoration: BoxDecoration(
                                color: HexColor('#F3F6FF'),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(23)),
                                boxShadow: const [
                                  // BoxShadow(blurRadius: 1.0),
                                  BoxShadow(
                                      blurRadius: 2.0,
                                      color: Colors.grey,
                                      offset: Offset(0, -2)),
                                  BoxShadow(
                                      blurRadius: 2.0,
                                      color: Colors.grey,
                                      offset: (Offset(2, 0)))
                                ]),
                            // color: HexColor('#F3F6FF'),
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                      padding:
                                          EdgeInsets.only(left: 25, top: 12),
                                      child: Text(
                                        'Frequeny',
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w600,
                                            color:
                                                Colors.black.withOpacity(0.7)),
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: CustomRadioButton(
                                    enableButtonWrap: true,
                                    enableShape: true,
                                    horizontal: false,
                                    // customShape: const CircleBorder(
                                    //     side: BorderSide(
                                    //         width: 0.7, color: Color(0xAA204289))),
                                    selectedBorderColor: HexColor('#204289'),
                                    unSelectedBorderColor: HexColor('#204289'),
                                    // autoWidth: true,
                                    // width: 150,
                                    width: 130,

                                    // height: 88,
                                    padding: 1,
                                    defaultSelected: 'One Time a Day',
                                    elevation: 5,
                                    absoluteZeroSpacing: false,
                                    unSelectedColor:
                                        Theme.of(context).canvasColor,
                                    // unSelectedColor: Colors.white,
                                    buttonLables: const [
                                      'One Time a Day',
                                      '2 Times a Day',
                                      '3 Times a Day',
                                      'Before Bed',
                                      'SoS'
                                    ],
                                    buttonValues: const [
                                      'One Time a Day',
                                      '2 Times a Day',
                                      '3 Times a Day',
                                      'Before Bed',
                                      'SoS'
                                    ],
                                    buttonTextStyle: const ButtonTextStyle(
                                        selectedColor: Colors.white,
                                        unSelectedColor: Colors.black,
                                        textStyle: TextStyle(fontSize: 11)),
                                    radioButtonValue: (value) {
                                      print(value);
                                      setState(() {
                                        frequency = value as String;
                                      });
                                    },
                                    selectedColor: HexColor('#204289'),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 10.0, top: 1),
                                  child: Align(
                                    alignment: Alignment.bottomRight,
                                    child: Text(
                                      '(optional)',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),

                          const SizedBox(
                            height: 10,
                          ),

                          // ##################################
                          // Submit button
                          Container(
                            height: 51,
                            // width: double.infinity,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          HexColor('#F46524')),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),

                                    // side: BorderSide(color: Colors.red)
                                  ))),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  await FirebaseFirestore.instance
                                      .collection('users')
                                      .doc('${auth!.email}')
                                      .collection('medication')
                                      .add({
                                    'date': DateTime.now(),
                                    'medication_name': getVal,
                                    'dose': doseController.text,
                                    'type': doseType,
                                    'dose_times': frequency,
                                    'admin_verified': false
                                  }).then((value) async {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        backgroundColor: Colors.green,
                                        duration: Duration(seconds: 5),
                                        content:
                                            Text('Data Submited Successfully'),
                                      ),
                                    );
                                    // await Future.delayed(const Duration(seconds: 2));
                                    await Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                Footer(
                                                  landingIndex:
                                                      2, // 3 denotes to show reports page
                                                )));
                                  });
                                }
                              },
                              child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'Save',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          )
                        ],
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
