import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/medication/main_med.dart';
import 'package:roojh/medication/readmore.dart';

class Medication extends StatefulWidget {
  const Medication({Key? key}) : super(key: key);

  @override
  State<Medication> createState() => _MedicationState();
}

class _MedicationState extends State<Medication> {
  User? user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(),
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundImage: Image.network("${user!.photoURL}").image,
            ),
          ),
        ],
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        title: const Center(
          child: Text(
            "Medication",
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 22, right: 22),
              child: Container(
                width: double.infinity,
                height: 51,
                child: ElevatedButton(
                  onPressed: () async {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const MainMed()));
                  },
                  child: const Text('UPDATE',
                      style: TextStyle(fontSize: 20, color: Colors.white)),
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.black),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(HexColor("#F46524")),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.9),

                        // side: BorderSide(color: Colors.red)
                      ))),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            StreamBuilder(
              // shrinkWrap: true,

              stream: FirebaseFirestore.instance
                  .collection('users')
                  .doc('${user!.email}')
                  .collection('medication')
                  .orderBy('date', descending: true)
                  .snapshots(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  return Flexible(
                    child: ListView(
                      children: snapshot.data!.docs.map((usersdata) {
                        var list1 = usersdata.get("dose_times");
                        String? dose_times;

                        return ListTile(
                          title: TextButton(
                            style: TextButton.styleFrom(
                              // shadowColor: Colors.white,
                              minimumSize: Size.zero,
                              backgroundColor:
                                  const Color.fromARGB(255, 255, 249, 249),
                              padding: EdgeInsets.zero,
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            ),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      MedReadMore(usersdata: usersdata)));
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(top: 25.0),
                              child: Container(
                                child: Column(children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        '${usersdata["medication_name"]}',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black,
                                            fontSize: 16),
                                      ),
                                      SizedBox(
                                          child: SvgPicture.asset(
                                        'icons/threedotted.svg',
                                        height: 6,
                                        width: 6,
                                      ))
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      '${usersdata["dose"]} ${usersdata["type"]}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 15,
                                          color: Colors.black.withOpacity(0.5)),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          list1.toString(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15,
                                              color: Colors.black
                                                  .withOpacity(0.5)),
                                        ),
                                      ),
                                      Container(
                                        child: usersdata['admin_verified']
                                            ? Column(
                                                children: [
                                                  Icon(
                                                    Icons.verified_user,
                                                    color: Colors.green,
                                                    size: 20,
                                                  ),
                                                  Text(
                                                    'Verified',
                                                    style:
                                                        TextStyle(fontSize: 10),
                                                  )
                                                ],
                                              )
                                            : Column(
                                                children: [
                                                  Icon(
                                                    Icons.dangerous_rounded,
                                                    color: Colors.redAccent,
                                                    size: 22,
                                                  ),
                                                  Text(
                                                    'Not Verified',
                                                    style:
                                                        TextStyle(fontSize: 10),
                                                  )
                                                ],
                                              ),
                                      )
                                    ],
                                  ),
                                ]),
                                padding: EdgeInsets.only(
                                    top: 10, left: 19, right: 18),
                                width: double.infinity,
                                height: 140,
                                decoration: BoxDecoration(
                                  color: HexColor('#F4F5F9'),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(9)),
                                  border: Border.all(
                                      color: Colors.black.withOpacity(0.4),
                                      width: 1),
                                ),
                              ),
                            ),
                          ),
                          // leading: Image.network(data[index]["body"]),
                        );
                      }).toList(),
                    ),
                  );
                } else if (!snapshot.hasData) {
                  return Center(
                    child: Text('No Data in '),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
            SizedBox(
              height: 25,
            )
          ],
        ),
      ),
    );
  }
}
