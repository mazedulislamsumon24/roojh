import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class LineChart extends StatefulWidget {
  String filterValue;
  String zoomValue;
  String title;
  String? svgPath;
  String valuesUnit;
  List<ChartData> chartData = <ChartData>[];

  LineChart(
      {Key? key,
      required this.filterValue,
      required this.zoomValue,
      required this.title,
      required this.svgPath,
      required this.chartData,
      required this.valuesUnit})
      : super(key: key);

  @override
  State<LineChart> createState() => _LineChartState();
}

class _LineChartState extends State<LineChart> {
  // ignore: non_constant_identifier_names
  DateTime todaysDate = DateTime.now();

  Map<String, double> zoomValue_map = {'100%': 0, '150%': 100, '200%': 200};
  Map<String, double> x_axis_interval = {
    'D': 0.1,
    'W': 0.05,
    'M': 0.08,
    '6M': 0.1,
    'Y': 0.1
  };
  Map<String, int> days_less = {'D': 1, 'W': 7, 'M': 31, '6M': 180, 'Y': 365};
  Map<String, dynamic> date_format = {
    'D': DateFormat.jm(),
    'W': DateFormat.E(),
    'M': DateFormat.MMMd(),
    '6M': DateFormat.MMM(),
    'Y': DateFormat.MMM()
  };

  Widget _buildLineChart() {
    return SfCartesianChart(
      title: ChartTitle(text: ''),
      primaryXAxis: DateTimeAxis(

          // to fix the crop of initial and ending value we must implement solution mentioned here. But to do this we need to have some solid data, hence parking this
          // https://www.syncfusion.com/forums/160997/columnseries-column-is-cropped-in-half-at-the-start-and-end-of-the-datetime-axis

          dateFormat: date_format[widget.filterValue],
          isVisible: true,
          edgeLabelPlacement: EdgeLabelPlacement.shift,
          interval: x_axis_interval[widget.filterValue],
          visibleMinimum: todaysDate
              .subtract(Duration(days: days_less[widget.filterValue] ?? 0))
              .add(Duration(hours: days_less[widget.filterValue] ?? 0)),
          //visibleMinimum: todaysDate.subtract(Duration(hours: 17)) ,
          visibleMaximum: todaysDate
              .add(Duration(hours: (days_less[widget.filterValue] ?? 0)) ~/ 2),
          //visibleMaximum: todaysDate.add(Duration(hours: 7)),
          //visibleMinimum: timestamp,

          //intervalType:'IntervalType.Years',
          //labelFormat: 'yMMM',

          majorGridLines: const MajorGridLines(width: 0.6)),
      primaryYAxis: NumericAxis(
        majorGridLines: MajorGridLines(
            width: 0.1, color: Color.fromARGB(255, 115, 114, 114)),
        interval: 50,
        rangePadding: ChartRangePadding.round,
        // maximum: 150,
        minimum: 50,
        axisLine: const AxisLine(
          width: 0,
        ),
        majorTickLines: const MajorTickLines(color: Colors.transparent),
      ),
      series: <ChartSeries<ChartData, Timestamp>>[
        ScatterSeries<ChartData, Timestamp>(
            name: widget.valuesUnit.replaceAll('(', "").replaceAll(')', ""),
            animationDuration: 1500,
            color: Color(0XFFF46524),
            dataSource: widget.chartData,
            xValueMapper: (ChartData data, _) => data.xTime,
            yValueMapper: (ChartData data, _) => data.yValue,
            pointColorMapper: (ChartData data, _) =>
                Color.fromARGB(255, 85, 83, 83),

            // width: 2,

            markerSettings: MarkerSettings(
              height: 9.5 + (zoomValue_map[widget.zoomValue] ?? 0) / 50,
              width: 9.5 + (zoomValue_map[widget.zoomValue] ?? 0) / 50,
              shape: DataMarkerType.diamond,
            )),
      ],
      tooltipBehavior: TooltipBehavior(enable: true, format: 'point.y '),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.6),
            spreadRadius: 8,
            blurRadius: 6,
            offset: const Offset(5, 5),
          ),
        ],
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          Row(
            children: [
              widget.svgPath != null
                  ? SizedBox(
                      child: SvgPicture.asset(
                      widget.svgPath.toString(),
                      width: 21,
                      height: 21,
                      color: Colors.red.shade800,
                    ))
                  : Container(),
              SizedBox(
                width: 3,
              ),
              Text(widget.title),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          FittedBox(
            child: SizedBox(
              child: _buildLineChart(),
              height: 170 + (zoomValue_map[widget.zoomValue] ?? 0),
              width: 400,
            ),
          ),
        ],
      ),
    );
  }
}

class ChartData {
  ChartData({this.yValue, this.xTime});

  final int? yValue;

  final Timestamp? xTime;
}
