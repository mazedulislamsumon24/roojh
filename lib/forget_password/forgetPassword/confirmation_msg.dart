import 'package:flutter/material.dart';

class ResetPasswordSent extends StatelessWidget {
  const ResetPasswordSent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Resend Password Has Been Sent'),
      ),
    );
  }
}
