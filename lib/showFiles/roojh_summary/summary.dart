import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../summary_pdf/pdf/pdf_page.dart';

class SummaryData {
  SummaryData(
      {this.specialist_doctor,
      this.allergies,
      this.medication,
      this.cardiology,
      this.family_history,
      this.medical_history,
      this.preferred_pharmacist,
      this.primary_doctor,
      this.radiology,
      this.recent_labs,
      this.social_history,
      this.surgical_history,
      this.comment});
  final List? allergies;

  final Map? cardiology;
  final Map? family_history;
  final Map? medical_history;
  final Map? preferred_pharmacist;
  final Map? primary_doctor;
  final Map? radiology;
  final List? recent_labs;
  final List? social_history;
  final List? medication;
  final Map? surgical_history;
  final Map? specialist_doctor;
  final String? comment;
}

class Summary extends StatefulWidget {
  const Summary({Key? key}) : super(key: key);

  @override
  State<Summary> createState() => _SummaryState();
}

class _SummaryState extends State<Summary> {
  List summaryList = [];
  Future<void> getSummary() async {
    var datas = await FirebaseFirestore.instance
        .collection('users')
        .doc(user!.email)
        .collection('roojh_summary')
        .get(GetOptions(source: Source.serverAndCache));
    List<SummaryData> getSnapshot = datas.docs
        .map((e) => SummaryData(
              social_history: e.data()['social_history'],
              surgical_history: e.data()['surgical_history'],
              allergies: e.data()['allergies'],
              cardiology: e.data()['cardiology'],
              medical_history: e.data()['medical_history'],
              radiology: e.data()['radiology'],
              recent_labs: e.data()['recent_labs'],
              preferred_pharmacist: e.data()['preferred_pharmacist'],
              primary_doctor: e.data()['primary_doctor'],
              family_history: e.data()['family_history'],
              medication: e.data()['medication'],
              specialist_doctor: e.data()['specialist_doctor'],
              comment: e.data()['comment'],
            ))
        .toList();

    setState(() {
      summaryList = getSnapshot;
      clicked = true;
      print(summaryList.first.radiology['Xray']);
    });
  }

  bool clicked = false;

  User? user = FirebaseAuth.instance.currentUser;
  @override
  void initState() {
    getSummary();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              if (Navigator.canPop(context)) {
                Navigator.pop(context);
              } else {
                SystemNavigator.pop();
              }
            }, // Handle your on tap here.
            icon: SvgPicture.asset('icons/arrow - right.svg'),
          ),
          iconTheme: const IconThemeData(color: Colors.black),

          backgroundColor: Colors.white,
          // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text(
            "Roojh Summary",
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.w600, fontSize: 16),
          ),
          actions: [
            // IconButton(
            //     onPressed: () {
            //       Navigator.of(context).push(MaterialPageRoute(
            //           builder: (BuildContext context) => PdfPage()));
            //     },
            //     icon: const Icon(Icons.save))
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    //container for no space in color
                    height: 90,
                    color: HexColor('#CED3E1'),
                    child: Container(
                      //name and image container
                      height: 87,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.blue.shade900,
                        borderRadius: const BorderRadius.only(
                          bottomLeft: const Radius.circular(10),
                          bottomRight: const Radius.circular(10),
                          // topLeft: Radius.circular(10),
                          // topRight: Radius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 25,
                    top: 18,
                    child: SvgPicture.asset(
                      'svg_png/roojhWhite.svg',
                      height: 31,
                    ),
                  ),
                  Positioned(
                    left: 100,
                    top: 50,
                    child: Text(
                      'Empowering Personal Health',
                      style: TextStyle(
                          color: Colors.white.withOpacity(0.95),
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  // arrow position
                  Positioned(
                      top: 80,
                      left: 20,
                      child: SvgPicture.asset(
                        'icons/arrowsummery.svg',
                        color: HexColor('#DEE7FF'),
                        width: 30,
                      )),

                  // box for profile
                ],
              ),
              Container(
                  //container for other details
                  height: 90,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: HexColor('#DEE7FF'),
                    borderRadius: const BorderRadius.only(
                        bottomLeft: const Radius.circular(10),
                        bottomRight: const Radius.circular(10)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(10), // Image border
                              child: SizedBox.fromSize(
                                size: const Size.fromRadius(25), // Image radius
                                child: user!.photoURL != null
                                    ? Image.network(user!.photoURL.toString(),
                                        fit: BoxFit.cover)
                                    : Image.network(
                                        'https://source.unsplash.com/72x72/?man',
                                        fit: BoxFit.cover),
                              ),
                            ),
                            Text(
                              user!.displayName!.isEmpty
                                  ? 'Your Name'
                                  : user!.displayName.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 13),
                            )
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, top: 10, right: 10),
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'DOB',
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black
                                                    .withOpacity(0.33)),
                                          ),
                                          const Text(
                                            '12/09/1980',
                                            style:
                                                const TextStyle(fontSize: 12),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, top: 10, right: 10),
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Email',
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black
                                                    .withOpacity(0.33)),
                                          ),
                                          Text(user!.email.toString(),
                                              style:
                                                  const TextStyle(fontSize: 12))
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, top: 10, right: 10),
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Address',
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black
                                                    .withOpacity(0.33)),
                                          ),
                                          const Text('Noida , India',
                                              style: TextStyle(fontSize: 12))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, top: 10, right: 10),
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Mobile No.',
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black
                                                    .withOpacity(0.33)),
                                          ),
                                          const Text('+91 9807416095',
                                              style: TextStyle(fontSize: 12))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, left: 1, right: 1),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border:
                          Border.all(color: Colors.blue.shade900, width: 5)),
                  child: Column(
                    children: [
                      Container(
                          height: 20,
                          width: double.infinity,
                          color: Colors.blue.shade900,
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              'Summary',
                              style: TextStyle(color: Colors.white),
                            ),
                          )),
                      clicked
                          ? Container(
                              // height: 450,
                              child: SummaryText(summaryList: summaryList),
                            )
                          : const SizedBox(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator()),
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}

class SummaryText extends StatelessWidget {
  const SummaryText({
    Key? key,
    required this.summaryList,
  }) : super(key: key);

  final List summaryList;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Medical History Map
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Text(
                    'Medical History :',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${summaryList.first.medical_history["description"]} ',
                        style: TextStyle(color: Colors.black.withOpacity(0.8)),
                      ),
                      for (var listValue
                          in summaryList.first.medical_history["problem_list"])
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(' - $listValue'))
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Surgical history
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Text(
                    'Surgical History :',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          '${summaryList.first.surgical_history["description"]} ',
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.8)),
                        ),
                      ),
                      for (var listValue
                          in summaryList.first.surgical_history["surgeries"])
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(' - $listValue'))
                    ],
                  ),
                ),
              ],
            ),
          ), //Family history
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Family History :',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w600),
                ),
                for (var listKey in summaryList.first.family_history.keys) ...[
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text('${listKey} :'),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (var listvalue2
                                in summaryList.first.family_history[listKey])
                              Text('${listvalue2}')
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  )
                ],
              ],
            ),
          ),
          // Social History
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Text(
                    'Social History :',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      for (var i in summaryList.first.social_history)
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '${i}',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.8)),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //alergies list
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Text(
                    'Allergies :',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      for (var i in summaryList.first.allergies)
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '${i}',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.8)),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Medication
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Text(
                    'Medication :',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      for (var i in summaryList.first.medication)
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '${i}',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.8)),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Recent Lebs
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Text(
                    'Recent Labs :',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      for (var i in summaryList.first.recent_labs)
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '${i}',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.8)),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Radiology
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Radiology :',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w600),
                        ),
                      ),
                      for (var listKey in summaryList.first.radiology.keys) ...[
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(child: Text("${listKey.toString()} :")),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  for (var subKey in summaryList
                                      .first.radiology[listKey].keys) ...[
                                    subKey != 'Date'
                                        ? Text(
                                            '$subKey: ${summaryList.first.radiology[listKey][subKey]}')
                                        : Text(
                                            '$subKey : ${(DateTime.parse(summaryList.first.radiology[listKey][subKey].toDate().toString()).day).toString()} - ${(DateTime.parse(summaryList.first.radiology[listKey][subKey].toDate().toString()).month).toString()} - ${(DateTime.parse(summaryList.first.radiology[listKey][subKey].toDate().toString()).year).toString()}\n'),
                                  ]
                                ],
                              ),
                            )
                          ],
                        ),
                      ]
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Cardiology
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Cardiology :',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w600),
                        ),
                      ),
                      for (var listKey
                          in summaryList.first.cardiology.keys) ...[
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(child: Text("${listKey.toString()} :")),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  for (var subKey in summaryList
                                      .first.cardiology[listKey].keys) ...[
                                    subKey != 'Date'
                                        ? Text(
                                            '$subKey: ${summaryList.first.cardiology[listKey][subKey]}')
                                        : Text(
                                            '$subKey : ${(DateTime.parse(summaryList.first.cardiology[listKey][subKey].toDate().toString()).day).toString()} - ${(DateTime.parse(summaryList.first.cardiology[listKey][subKey].toDate().toString()).month).toString()} - ${(DateTime.parse(summaryList.first.cardiology[listKey][subKey].toDate().toString()).year).toString()}\n'),
                                  ]
                                ],
                              ),
                            )
                          ],
                        ),
                      ]
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Preferred Pharmacist
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Preferred Pharmacist :',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Name :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.preferred_pharmacist["name"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Telephone :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.preferred_pharmacist["telephone"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Address :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.preferred_pharmacist["address"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Primary doctor
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Primary Doctor :',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Name :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.primary_doctor["name"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Telephone :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.primary_doctor["telephone"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Address :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.primary_doctor["address"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Special Doctor
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Specialist Doctor   :',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Name :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.specialist_doctor["name"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Telephone :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.specialist_doctor["telephone"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Address :")),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${summaryList.first.specialist_doctor["address"]}')
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // comment
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment:
              // MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Comment  :',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                Expanded(child: Text(summaryList.first.comment.toString()))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
