import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/ReportsFrom/bpForm/bpform.dart';
import 'package:roojh/ReportsFrom/commonForm/commonCardForms.dart';
import 'package:roojh/ReportsFrom/multiselect.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:roojh/ReportsFrom/sugar_from/sugarForm.dart';

class SelectReportForm extends StatefulWidget {
  final List<String?>? items;
  SelectReportForm({Key? key, this.items}) : super(key: key);

  @override
  State<SelectReportForm> createState() => _SelectReportFormState();
}

class _SelectReportFormState extends State<SelectReportForm> {
  List<displayCardItem> listDatas = [];
  List<displayCardItem> listDatas2 = [];
  User? user = FirebaseAuth.instance.currentUser;
  // List<String?>? items;
  List allAvailableCards = [];
  List availableToSelectCards = [];
  bool status = false;
  bool ready_to_load = false;

  Future<void> getToSelectTiles() async {
    // ##################################
    //get data from db in list allAvailableCard
    var snapShotsValue1 = await FirebaseFirestore.instance
        .collection('users')
        .doc(user!.email.toString())
        .collection("tiles")
        .get();
    List<displayCardItem> listF = snapShotsValue1.docs
        .map((e) => displayCardItem(
              title: e.data()['title'],
              vital_db: e.data()['vital_db'],
              value: e.data()['value'],
              routeToWidget: e.data()['routeToWidget'],
              valuesUnit: e.data()['valuesUnit'],
            ))
        .toList();
    setState(() {
      listDatas = listF;
// add data form listDatas in allAvailableCards
      for (var a in listDatas) {
        allAvailableCards.add(displayCardItem(
          title: a.title,
          vital_db: a.vital_db,
          routeToWidget: a.routeToWidget,
          value: a.value,
          valuesUnit: a.valuesUnit,
        ));
      }
    });
    // ##########################
    //get data from db in list availableToSelectCards
    var snapShotsValue2 =
        await FirebaseFirestore.instance.collection("toselect").get();
    List<displayCardItem> list2 = snapShotsValue2.docs
        .map((e) => displayCardItem(
              title: e.data()['title'],
              vital_db: e.data()['vital_db'],
              value: e.data()['value'],
              routeToWidget: e.data()['routeToWidget'],
              valuesUnit: e.data()['valuesUnit'],
            ))
        .toList();
    setState(() {
      listDatas2 = list2;
      // ##########################
      //get data from listDatas2 in list availableToSelectCards
      for (var a in listDatas2) {
        availableToSelectCards.add(displayCardItem(
          title: a.title,
          vital_db: a.vital_db,
          routeToWidget: a.routeToWidget,
          value: a.value,
          valuesUnit: a.valuesUnit,
        ));
      }
      ready_to_load = true;
    });
  }

// to delete from db
  deleteFromCard(get, collNameToRemove) async {
    print("_____iid_____${get}");
    await FirebaseFirestore.instance
        .collection('users')
        .doc(user!.email.toString())
        .collection(collNameToRemove)
        .doc(get.first.vital_db)
        .delete();
  }

  // To crete data in one db
  createDbForCard(get, collNameToAdd) async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(user!.email.toString())
        .collection(collNameToAdd)
        .doc(get.first.vital_db)
        .set({
      'title': get.first.title,
      'vital_db': get.first.vital_db,
      'value': get.first.value,
      'valuesUnit': get.first.valuesUnit,
      'routeToWidget': get.first.routeToWidget
    });
  }

  // ############################################
// select items list in order to add user card tiles
  void _showMultiSelect() async {
    // using set to get uncommon list data from toSelect db and user tiles so we can show in multiselect list only which is not selected
    var multiitems =
        availableToSelectCards.map((e) => e.title.toString()).toList();
    var usersCard = allAvailableCards.map((e) => e.title.toString()).toList();
    var setMultiitems = multiitems.toSet();
    var setusersCard = usersCard.toSet();

    var nonCommonSet = setMultiitems
        .union(setusersCard)
        .difference(setMultiitems.intersection(setusersCard));
    // uncommon datas from list which is coming from db
    var nonCommonList = nonCommonSet.toList()..sort();
    print('non common list $nonCommonList');
    // a list of selectable items
    // these items can be hard-coded or dynamically fetched from a database/API

    final List<String>? results = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return MultiSelect(items: nonCommonList);
      },
    );

    // Update UI
    if (results != null) {
      setState(() {
        for (var fromRes in results) {
          int index = availableToSelectCards
              .indexWhere((item) => item.title == fromRes);

          // ##########################################
          // for add and  delete from one db to other
          var get = availableToSelectCards.where((item) {
            var a = item.title == fromRes;
            return a;
          }).toList();
          // var collNameToRemove = 'toselect';
          // deleteFromCard(get, collNameToRemove);
          var collNameToAdd = 'tiles';
          createDbForCard(get, collNameToAdd);

          // ##########################################
          // for add and  delete from one db to other END
          // print("_____iid_____${get.first.title}");
          allAvailableCards.add(availableToSelectCards[index]);
          availableToSelectCards.removeAt(index);

          print("__________$index");
        }
      });
    }
  }

  Widget buildDisplayCard(item) {
    return InkWell(
      onTap: () {
        if (item.title.toString() != 'Blood Pressure' &&
            item.title.toString() != 'Blood Glucose') {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => CommonCardForm(
                  title: item.title,
                  valuesUnit: item.valuesUnit,
                  vital_db: item.vital_db)));
        } else if (item.title.toString() == 'Blood Pressure') {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => BPForm()));
        } else if (item.title.toString() == 'Blood Glucose') {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => SugarForm()));
        }
      },
      child: Stack(
        fit: StackFit.expand,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 8.0, bottom: 3, left: 3, right: 3),
            child: SizedBox(
              height: 103,
              width: 110,
              child: Container(
                  // height: 103,
                  // width: 110,
                  decoration: BoxDecoration(
                      color: HexColor('#F4F5F9'),
                      borderRadius:
                          const BorderRadius.all(const Radius.circular(23)),
                      boxShadow: const [
                        // BoxShadow(blurRadius: 1.0),
                        BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.grey,
                            offset: Offset(0, -3)),
                        BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.grey,
                            offset: (Offset(3, 0)))
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 6,
                      right: 8,
                      top: 5,
                    ),
                    child: Column(
                      children: [
                        Text(
                          item.title,
                          style: const TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w600),
                        ),
                        const SizedBox(
                          height: 11,
                        ),
                        Text(
                          item.value,
                          style: const TextStyle(fontSize: 14),
                        ),
                        const SizedBox(
                          height: 1,
                        ),
                        Text(
                          item.valuesUnit,
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black.withOpacity(0.5)),
                        ),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: SvgPicture.asset('icons/plus.svg')),
                      ],
                    ),
                  )),
            ),
          ),
          // remove button
          status
              ? Positioned(
                  top: -12,
                  left: -14.5,
                  child: IconButton(
                    iconSize: 23,
                    icon: Icon(Icons.remove_circle),
                    onPressed: () {
                      int index = allAvailableCards
                          .indexWhere((e) => e.title == item.title);
                      setState(() {
                        // ##########################################
                        // for add and  delete from one db to other
                        var get = allAvailableCards.where((e) {
                          var a = item.title == e.title;
                          return a;
                        }).toList();
                        var collNameToRemove = 'tiles';
                        deleteFromCard(get, collNameToRemove);
                        // var collNameToAdd = 'toselect';
                        // createDbForCard(get, collNameToAdd);
                        // deleteFromCard(get, collNameToRemove);
                        // ##########################################
                        // for add and  delete from one db to other END
                        print("_____iid2_____${get.first.routeToWidget}");
                        availableToSelectCards.add(allAvailableCards[index]);
                        allAvailableCards.removeAt(index);
                      });
                    },
                  ))
              : SizedBox()
        ],
      ),
    );
  }

  @override
  void initState() {
    getToSelectTiles();

    // items = widget.items;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(),
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundImage: Image.network("${user!.photoURL}").image,
            ),
          ),
        ],
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Center(
          child: Text(
            "Take Readings",
            style: TextStyle(
                color: Colors.black87,
                fontSize: 16,
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
      body: ready_to_load
          ? SingleChildScrollView(
              child: Center(
              child: Column(
                children: [
                  const SizedBox(
                    height: 50,
                  ),
                  const NextReadingCard(),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.blue.shade900),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.9),

                              // side: BorderSide(color: Colors.red)
                            ))),
                        child: const Text('Select Tiles'),
                        onPressed: () {
                          _showMultiSelect();
                        },
                      ),
                      Container(
                          child: FlutterSwitch(
                        activeColor: Colors.blue.shade900,
                        inactiveColor: HexColor('#F46524'),
                        width: 110.0,
                        inactiveText: 'Remove Tiles',
                        height: 35.0,
                        valueFontSize: 12.0,
                        toggleSize: 15.0,
                        value: status,
                        borderRadius: 30.0,
                        padding: 8.0,
                        showOnOff: true,
                        onToggle: (val) {
                          setState(() {
                            status = val;
                          });
                        },
                      ))
                    ],
                  ),
                  const Divider(
                    height: 30,
                  ),

                  const SizedBox(
                    height: 20,
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left: 3.0, right: 3),
                    child: GridView.count(
                      primary: false,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: 2,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 20,
                      childAspectRatio: 1.65,
                      children: allAvailableCards
                          .map((e) => buildDisplayCard(e))
                          .toList(),
                    ),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(right: 5, left: 5),
                  //   child: Column(
                  //     children: allAvailableCards
                  //         .map((e) => buildDisplayCard(e))
                  //         .toList(),
                  //   ),
                  // ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ))
          : Center(child: CircularProgressIndicator()),
    );
  }
}

class NextReadingCard extends StatelessWidget {
  const NextReadingCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 7.0, left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  // mainAxisSize: MainAxisSize.min,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 9),
                      child: Text('Est. HbA1c '),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          'icons/blood-drop.svg',
                          width: 18,
                          height: 18,
                        ),
                        const SizedBox(
                          width: 2,
                        ),
                        const Text('7.6 %        ')
                      ],
                    ),
                    const Text('.............       '),
                    const SizedBox(
                      height: 2,
                    ),
                    const Text('Steps          '),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          'icons/footsteps.svg',
                          width: 18,
                          height: 18,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text('0          '),
                      ],
                    ),
                    const Text('.............       '),
                    const SizedBox(
                      height: 2,
                    ),
                    const Text('Cal             '),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.bloodtype,
                          color: Colors.red.shade900,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text('0          '),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0, right: 30),
                child: Column(
                  children: [
                    const Text(
                      ' 05 Jun',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      'BMI',
                      style: TextStyle(
                          // fontSize: 40,
                          color: HexColor('#F46524'),
                          fontWeight: FontWeight.w600),
                    ),
                    Text(
                      '20.1',
                      style: TextStyle(
                          fontSize: 40,
                          color: HexColor('#F46524'),
                          fontWeight: FontWeight.w600),
                    ),
                    const Text('kg/m2 '),
                    const SizedBox(
                      height: 13,
                    ),
                    Text(
                      'Normal',
                      style: TextStyle(
                          color: HexColor('#F46524'),
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          ),
          height: 200,
          width: 290,
          decoration: BoxDecoration(
              color: HexColor('#F4F5F9'),
              borderRadius: const BorderRadius.all(Radius.circular(23)),
              boxShadow: const [
                // BoxShadow(blurRadius: 1.0),
                BoxShadow(
                    blurRadius: 5.0, color: Colors.grey, offset: Offset(0, -5)),
                BoxShadow(
                    blurRadius: 5.0, color: Colors.grey, offset: (Offset(5, 0)))
              ]),
        ),
        Positioned(
            left: 60,
            top: 170,
            child: ElevatedButton(
              style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(HexColor('#F46524')),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.9),

                    // side: BorderSide(color: Colors.red)
                  ))),
              child: const Text('Take Your Next Reading'),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => const HomePage()),
                // );
              },
            )),
      ],
    );
  }
}

class displayCardItem {
  String title;
  // String svgPath;
  String value;
  String valuesUnit;
  String vital_db;
  String routeToWidget;

  displayCardItem(
      {required this.title,
      required this.vital_db,
      // required this.svgPath,
      required this.value,
      required this.valuesUnit,
      required this.routeToWidget});
}
