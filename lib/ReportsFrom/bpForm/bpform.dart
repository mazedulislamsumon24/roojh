import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/common_code/footer.dart';

import 'package:roojh/reports_page/reports_page.dart';

class BPForm extends StatefulWidget {
  const BPForm({Key? key}) : super(key: key);

  @override
  State<BPForm> createState() => _BPFormState();
}

class _BPFormState extends State<BPForm> {
  final _formKey = GlobalKey<FormState>();
  final sys = TextEditingController();
  final dia = TextEditingController();
  final auth = FirebaseAuth.instance.currentUser;
  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2020, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<void> updateTiles(int sys, int dia) async {
    FirebaseFirestore.instance
        .collection('users')
        .doc('${auth!.email}')
        .collection('tiles')
        .doc('blood_pressure')
        .update({'value': '${sys}/${dia}'});
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<void> addUser(int sys, int dia) async {
    return users
        .doc('${auth!.email}')
        .collection('vitals')
        .doc('blood_pressure')
        .collection('blood_pressure')
        .add({'date': selectedDate, 'sys': sys, 'dia': dia}).then(
            (value) async {
      await updateTiles(sys, dia);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Colors.green,
          duration: Duration(seconds: 5),
          content: Text('Data Submited Successfully'),
        ),
      );
      // await Future.delayed(const Duration(seconds: 2));
      await Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => Footer(
                landingIndex: 3, // 3 denotes to show reports page
              )));
    }).catchError((error) => ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                backgroundColor: Colors.red,
                content: Text(
                  'Something Went Wrong',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: Drawer(),
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            if (Navigator.canPop(context)) {
              Navigator.pop(context);
            } else {
              SystemNavigator.pop();
            }
          }, // Handle your on tap here.
          icon: SvgPicture.asset('icons/arrow - right.svg'),
        ),
        iconTheme: const IconThemeData(color: Colors.black),

        backgroundColor: Colors.white,
        // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          "Blood Pressure",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Container(
                      child: Column(children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Systolic',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.red.shade600),
                            )),
                        SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 100,
                              child: TextFormField(
                                // obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w600),
                                decoration: InputDecoration(
                                  hintText: "120",
                                  //create lable
                                  hintStyle: TextStyle(
                                      color: Colors.black.withOpacity(0.2)),
                                  filled: true,
                                  fillColor: HexColor('#F3F6FF'),
                                  // border: InputBorder.none,
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.0)),
                                  ),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(30.0, 15.0, 20.0, 0),

                                  // hintText: "120",
                                ),
                                controller: sys,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'please enter Systolic Value';
                                  }
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 32, left: 10),
                              child: Text(
                                'mmHg',
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.5)),
                              ),
                            )
                          ],
                        )
                      ]),
                      padding: EdgeInsets.all(22),
                      height: 165,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: HexColor('#F3F6FF'),
                          borderRadius: BorderRadius.all(Radius.circular(23)),
                          boxShadow: const [
                            // BoxShadow(blurRadius: 1.0),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: Offset(0, -5)),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: (Offset(5, 0)))
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Container(
                      child: Column(children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Diastolic',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.red.shade600),
                            )),
                        SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 100,
                              child: TextFormField(
                                // obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w600),
                                decoration: InputDecoration(
                                  hintText: "60",
                                  //create lable
                                  hintStyle: TextStyle(
                                      color: Colors.black.withOpacity(0.2)),
                                  filled: true,
                                  fillColor: HexColor('#F3F6FF'),
                                  // border: InputBorder.none,
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.0)),
                                  ),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(30.0, 15.0, 20.0, 0),

                                  // hintText: "120",
                                ),
                                controller: dia,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'please enter Diastolic Value';
                                  }
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 32, left: 10),
                              child: Text(
                                'mmHg',
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.5)),
                              ),
                            )
                          ],
                        )
                      ]),
                      padding: EdgeInsets.all(22),
                      height: 165,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: HexColor('#F3F6FF'),
                          borderRadius: BorderRadius.all(Radius.circular(23)),
                          boxShadow: [
                            // BoxShadow(blurRadius: 1.0),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: Offset(0, -5)),
                            BoxShadow(
                                blurRadius: 5.0,
                                color: Colors.grey,
                                offset: (Offset(5, 0)))
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      height: 51,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: HexColor('#F3F6FF'),
                        borderRadius: BorderRadius.circular(98),
                        border: Border.all(color: HexColor('#CED3E1')),
                        //   boxShadow: [
                        //     BoxShadow(
                        //       color: Color(0xffeeeeee),
                        //       blurRadius: 10,
                        //       offset: Offset(0, 4),
                        //     ),
                        //   ],
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18),
                            child: Text(
                              "${selectedDate.toLocal()}".split(' ')[0],
                              // style: TextStyle(fontSize: 15),
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 18),
                            child: TextButton(
                              onPressed: () => _selectDate(context),
                              child: Text(
                                'Select date',
                                style: TextStyle(color: HexColor('#F46524')),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 22, right: 22),
                    child: Container(
                      width: double.infinity,
                      height: 51,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await addUser(
                                int.parse(sys.text), int.parse(dia.text));
                          }
                        },
                        child: const Text('Save',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.black),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                HexColor('#F46524')),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.9),

                              // side: BorderSide(color: Colors.red)
                            ))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
