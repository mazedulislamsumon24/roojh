import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/ReportsFrom/select_report.dart';
import 'package:roojh/common_code/footer.dart';
import 'package:roojh/reports_page/bp_report.dart';
import 'package:intl/intl.dart';
import 'generic_chat_widget/Linechart.dart' show ChartData;

import 'blood_glucose.dart';
import 'generic_chat_widget/Linechart.dart';

class _ChartData {
  _ChartData({this.value, this.time});

  final int? value;

  final Timestamp? time;
}

class LineChartDb {
  String title;
  // String svgPath;
  String value;
  String valuesUnit;
  String vital_db;
  String routeToWidget;

  LineChartDb(
      {required this.title,
      required this.vital_db,
      // required this.svgPath,
      required this.value,
      required this.valuesUnit,
      required this.routeToWidget});
}

class ReportsPage extends StatefulWidget {
  const ReportsPage({Key? key}) : super(key: key);

  @override
  State<ReportsPage> createState() => _ReportsPageState();
}

class _ReportsPageState extends State<ReportsPage> {
  @override
  void initState() {
    // fetch the listOfgraphs from DB and apply on init state

    addGraph();
    super.initState();
  }

  User? user = FirebaseAuth.instance.currentUser;

  String filterValue = "M";
  String zoom_value = '100%';

  List<Widget> listOfGraphs = [];

  List<ChartData> chartData = [
    // ChartData(yValue: 85, xTime: Timestamp.now()),
    // ChartData(yValue: 90, xTime: Timestamp.now()),
    // ChartData(yValue: 81, xTime: Timestamp.now())
  ];
  bool ready_to_load = false;
  bool listEmpty = false;
  Future<void> addGraph() async {
    listOfGraphs.clear();
    // fetch the listOfgraphs from DB and apply on init state

    var gCollection = await FirebaseFirestore.instance
        .collection('users')
        .doc(user!.email.toString())
        .collection('tiles')
        .get(GetOptions(source: Source.serverAndCache));
    // print('gcollection ');
    List<LineChartDb> listF = gCollection.docs
        .map((e) => LineChartDb(
              title: e.data()['title'],
              vital_db: e.data()['vital_db'],
              value: e.data()['value'],
              routeToWidget: e.data()['routeToWidget'],
              valuesUnit: e.data()['valuesUnit'],
            ))
        .toList();
    ready_to_load = true;
    if (listF.length == 0) {
      setState(() {
        listEmpty = true;
      });
    }

    for (var a in listF) {
      if (a.vital_db.toString() == 'blood_pressure') {
        setState(() {
          listOfGraphs.add(
            BpReport(
                filterValue: filterValue,
                zoomValue: zoom_value,
                key: ObjectKey(filterValue + " ")),
          );
        });
      } else if (a.vital_db.toString() == 'blood_glucose') {
        setState(() {
          listOfGraphs.add(BloodGlucose(
            zoomValue: zoom_value,
            filterValue: filterValue,
            key: ObjectKey(filterValue + " "),
          ));
        });
      } else if (a.vital_db.toString() != 'blood_glucose' &&
          a.vital_db.toString() != 'blood_pressure') {
        listOfGraphs.add(LineChart(
          key: ObjectKey(filterValue + " "),
          svgPath: 'icons/${a.vital_db}.svg',
          filterValue: filterValue,
          zoomValue: zoom_value,
          title: a.title,
          valuesUnit: a.valuesUnit,
          chartData: await getDataFromFireStore(a.vital_db) as List<ChartData>,
        ));
      }
    }
  }

  updateListOfGraphs() async {
    listOfGraphs.clear();
    // fetch the updated Order from DB and update it from Below line
    var gCollection = await FirebaseFirestore.instance
        .collection('users')
        .doc(user!.email.toString())
        .collection('tiles')
        .get(GetOptions(source: Source.serverAndCache));

    List<LineChartDb> listF = gCollection.docs
        .map((e) => LineChartDb(
              title: e.data()['title'],
              vital_db: e.data()['vital_db'],
              value: e.data()['value'],
              routeToWidget: e.data()['routeToWidget'],
              valuesUnit: e.data()['valuesUnit'],
            ))
        .toList();
    ready_to_load = true;
    if (listF.length == 0) {
      setState(() {
        listEmpty = true;
      });
    }

    for (var a in listF) {
      if (a.vital_db.toString() == 'blood_pressure') {
        setState(() {
          listOfGraphs.add(
            BpReport(
                filterValue: filterValue,
                zoomValue: zoom_value,
                key: ObjectKey(filterValue + " ")),
          );
        });
      } else if (a.vital_db.toString() == 'blood_glucose') {
        setState(() {
          listOfGraphs.add(BloodGlucose(
            zoomValue: zoom_value,
            filterValue: filterValue,
            key: ObjectKey(filterValue + " "),
          ));
        });
      } else if (a.vital_db.toString() != 'blood_glucose' &&
          a.vital_db.toString() != 'blood_pressure') {
        listOfGraphs.add(LineChart(
          key: ObjectKey(filterValue + " "),
          svgPath: 'icons/${a.vital_db}.svg',
          filterValue: filterValue,
          zoomValue: zoom_value,
          title: a.title,
          valuesUnit: a.valuesUnit,
          chartData: await getDataFromFireStore(a.vital_db) as List<ChartData>,
        ));
      }
    }
  }

  DateTime todaysDate = DateTime.now();

  Map<String, int> days_less = {'D': 1, 'W': 7, 'M': 31, '6M': 180, 'Y': 365};

// for chartdata

  getDataFromFireStore(String vital_db) async {
    DateTime todaysDate_min =
        DateTime.now().subtract(Duration(days: days_less[filterValue] ?? 0));

    Timestamp timestamp = Timestamp.fromDate(todaysDate_min);
    var snapShotsValue = await FirebaseFirestore.instance
        .collection("users")
        .doc('${user!.email}')
        .collection('vitals')
        .doc(vital_db)
        .collection(vital_db)
        .where('date', isGreaterThanOrEqualTo: timestamp)
        .orderBy('date', descending: false)
        .get(GetOptions(source: Source.serverAndCache));

    List<_ChartData> list = snapShotsValue.docs
        .map(
            (e) => _ChartData(time: e.data()['date'], value: e.data()['value']))
        .toList();
    setState(() {
      chartData.clear();
      for (var a in list) {
        chartData.add(
          ChartData(yValue: a.value, xTime: a.time),
        );
      }
    });
    print('chartData Type $chartData');
    return chartData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(),
        appBar: AppBar(
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                backgroundImage: Image.network("${user!.photoURL}").image,
              ),
            ),
          ],
          iconTheme: const IconThemeData(color: Colors.black),

          backgroundColor: Colors.white,
          // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Center(
            child: Text(
              "Reports",
              style: TextStyle(color: Colors.black),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 183,
                    height: 34,
                    child: TextField(
                        style: const TextStyle(
                          fontSize: 13.0,
                          color: Colors.blueAccent,
                        ),
                        decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.fromLTRB(0.0, 8.0, 0, 15.0),
                            prefixIcon: Icon(
                              Icons.search,
                              color: HexColor('#204289'),
                            ),
                            hintText: "Search",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: HexColor('#F4F5F9'), width: 32.0),
                                borderRadius: BorderRadius.circular(17.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: HexColor('#F4F5F9'), width: 32.0),
                                borderRadius: BorderRadius.circular(17.0)))),
                  ),
                  Container(
                    child: DropdownButton<String>(
                      icon: SvgPicture.asset('icons/filter.svg'),
                      items:
                          <String>['100%', '150%', '200%'].map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          listOfGraphs.clear();
                          zoom_value = value!;
                          updateListOfGraphs();
                        });
                      },
                    ),
                  ),
                ],
              ),
              ElevatedButton(
                style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(HexColor('#F46524')),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.9),

                      // side: BorderSide(color: Colors.red)
                    ))),
                child: Text('Add more  +'),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => Footer(
                            landingIndex: 1,
                          )));
                },
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                child: FittedBox(
                  child: CustomRadioButton(
                    buttonTextStyle: const ButtonTextStyle(
                      selectedColor: Colors.white,
                      unSelectedColor: Colors.black,
                      textStyle: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    buttonLables: const [
                      "1 Day",
                      "7 Days",
                      "31 Days",
                      "12 Months"
                    ],
                    buttonValues: const ["D", "W", "M", "Y"],
                    spacing: 0,
                    defaultSelected: "M",
                    horizontal: false,
                    enableButtonWrap: false,
                    width: 96,
                    height: 35,
                    absoluteZeroSpacing: true,
                    selectedColor: const Color(0xFFF46524),
                    // padding: 1,
                    unSelectedColor: Colors.white,
                    // enableShape: true,
                    // autoWidth: true,
                    radioButtonValue: (value) {
                      setState(() {
                        listOfGraphs.clear();
                        filterValue = value.toString();
                        updateListOfGraphs();
                        print(filterValue);
                      });
                    },
                  ),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Tooltip(
                    triggerMode: TooltipTriggerMode.tap,
                    message: "Tap and hold the Graph tile to re-order",
                    child: Container(
                      width: 28,
                      height: 28,
                      decoration: BoxDecoration(
                        color: Colors.green.withOpacity(0.25), // border color
                        shape: BoxShape.circle,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(2), // border width
                        child: Container(
                          // or ClipRRect if you need to clip the content
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xFFF46524), // inner circle color
                          ),
                          child: const Center(
                              child: Text(
                            "i",
                            style: TextStyle(color: Colors.white),
                          )), // inner content
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              ReorderableListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: listOfGraphs.length,
                itemBuilder: (context, index) {
                  return listOfGraphs[index];
                },
                onReorder: (int oldIndex, int newIndex) {
                  setState(() async {
                    final index = newIndex > oldIndex ? newIndex - 1 : newIndex;
                    final graph = listOfGraphs.removeAt(oldIndex);

                    listOfGraphs.insert(index, graph);
                  });
                },
              )

              // BloodGlucose(
              //   zoomValue: zoom_value,
              //   filterValue: filterValue,
              //   key: ObjectKey(filterValue + " "),
              // ),
              // BpReport(
              //     filterValue: filterValue,
              //     zoomValue: zoom_value,
              //     key: ObjectKey(filterValue + " "))

              // const KidneyFunction(),
              // const WeightReport()
            ],
          ),
        ));
  }
}



  // addGraph() async {
  //   listOfGraphs = [
  //     BloodGlucose(
  //       zoomValue: zoom_value,
  //       filterValue: filterValue,
  //       key: ObjectKey(filterValue + " "),
  //     ),
  //     BpReport(
  //         filterValue: filterValue,
  //         zoomValue: zoom_value,
  //         key: ObjectKey(filterValue + " ")),
  //   ];
  //   // fetch the listOfgraphs from DB and apply on init state

  //   var gCollection = await FirebaseFirestore.instance
  //       .collection('users')
  //       .doc(user!.email.toString())
  //       .collection('tiles')
  //       .get();
  //   List<LineChartDb> listF = gCollection.docs
  //       .map((e) => LineChartDb(
  //             title: e.data()['title'],
  //             vital_db: e.data()['vital_db'],
  //             value: e.data()['value'],
  //             routeToWidget: e.data()['routeToWidget'],
  //             valuesUnit: e.data()['valuesUnit'],
  //           ))
  //       .toList();
  //   for (var a in listF) {
  //     if (a.title != 'Blood Pressure' || a.title != "Blood Glucose") {
  //       continue;
  //     }
  //     setState(() {
  //       listOfGraphs.add(LineChart(
  //           filterValue: filterValue,
  //           zoomValue: zoom_value,
  //           title: a.title,
  //           chartData: chartData,
  //           key: ObjectKey(filterValue + " ")));
  //     });
  //   }
  // }