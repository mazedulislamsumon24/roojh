import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class WeightReport extends StatefulWidget {
  const WeightReport({Key? key}) : super(key: key);

  @override
  State<WeightReport> createState() => _WeightReportState();
}

class _WeightReportState extends State<WeightReport> {
  User? user = FirebaseAuth.instance.currentUser;
  List<_SplineAreaData>? chartData = <_SplineAreaData>[];
  @override
  void initState() {
    // getDataFromFireStore().then((results) {
    //   SchedulerBinding.instance!.addPostFrameCallback((timeStamp) {
    //     setState(() {});
    //   });
    // });
    getDataFromFireStore();
    super.initState();
  }

  Future<void> getDataFromFireStore() async {
    var snapShotsValue = await FirebaseFirestore.instance
        .collection("usersWeightData")
        .doc('${user!.email}')
        .collection('WeightData')
        .orderBy('date', descending: true)
        .get();
    List<_SplineAreaData> list = snapShotsValue.docs
        .map((e) =>
            _SplineAreaData(time: e.data()['date'], weight: e.data()['weight']))
        .toList();
    setState(() {
      chartData = list;
      print('----------------------- ${list}');
    });
  }

  Widget _buildSplineAreaChart() {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      primaryXAxis: NumericAxis(
          isVisible: false,
          interval: 10,
          majorGridLines: const MajorGridLines(width: 0),
          edgeLabelPlacement: EdgeLabelPlacement.shift),
      primaryYAxis: NumericAxis(
        labelFormat: '{value}',
        desiredIntervals: 10,
        maximum: 10,
        minimum: 60,
      ),
      series: <ChartSeries<_SplineAreaData, num>>[
        SplineAreaSeries<_SplineAreaData, num>(
          dataSource: chartData!,
          borderColor: Color(0xFFF46524),
          color: Colors.transparent,
          borderWidth: 2,
          name: '',
          xValueMapper: (_SplineAreaData sales, _) => sales.time,
          yValueMapper: (_SplineAreaData sales, _) => sales.weight,
        ),
      ],
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.6),
            spreadRadius: 8,
            blurRadius: 6,
            offset: Offset(5, 5),
          ),
        ],
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: Text(
              "Weight",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: const [
                  Text("Value"),
                  Text(
                    "136lbs",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
              Text(
                "Ask the client to update the value",
                style: TextStyle(
                    color: Colors.blueAccent,
                    decoration: TextDecoration.underline),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
          FittedBox(
            child: SizedBox(
              child: _buildSplineAreaChart(),
              height: 150,
              width: 400,
            ),
          ),
          SizedBox(
            height: 20,
            width: 250,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(1),
                primary: Color(0XFF16B25E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(21.0),
                ),
              ),
              onPressed: () {},
              child: Text(
                "Already Added to dashboard",
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _SplineAreaData {
  _SplineAreaData({this.time, this.weight});
  final num? time;
  final num? weight;
}
