import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'dart:io';
import 'package:open_file/open_file.dart';
import 'package:printing/printing.dart';
import 'package:pdf/widgets.dart' as pw;

Future<Uint8List> generatedPdf(PdfPageFormat format) async {
  User? user = FirebaseAuth.instance.currentUser;
  // ##################################
  //get data from db in list allAvailableCard
  var snapShotsValue1 = await FirebaseFirestore.instance
      .collection('users')
      .doc('ankit.kothari@hotmail.com')
      .collection('roojh_summary')

      // .where('month', isEqualTo: months2.asMap())
      .get();
  final doc = pw.Document(title: "Roojh");
  final logoImage = pw.MemoryImage(
      (await rootBundle.load('assets/roojh_logo.png')).buffer.asUint8List());
  final footerImage = pw.MemoryImage(
      (await rootBundle.load('assets/footer.png')).buffer.asUint8List());
  final font = (await rootBundle.load('assets/OpenSans-Regular.ttf'));
  final ttf = pw.Font.ttf(font);
  // final pageTheme = await _myPageTheme(format);
  doc.addPage(pw.MultiPage(
      // pageTheme: pageTheme,
      header: (final context) => pw.Image(logoImage,
          alignment: pw.Alignment.topLeft, fit: pw.BoxFit.contain, width: 100),
      // footer: (final context) =>
      //     pw.Image(footerImage, fit: pw.BoxFit.scaleDown),
      build: (final context) => [
            pw.Container(
                padding: pw.EdgeInsets.only(left: 15, bottom: 15),
                child: pw.Column(
                    crossAxisAlignment: pw.CrossAxisAlignment.center,
                    mainAxisAlignment: pw.MainAxisAlignment.start,
                    children: [
                      pw.Padding(
                          padding: pw.EdgeInsets.only(top: 20),
                          child: pw.Column(children: [
                            pw.Stack(
                              // clipBehavior:Clip.none,
                              children: [
                                pw.Container(
                                  //name and image container
                                  height: 87,
                                  width: double.infinity,
                                  decoration: pw.BoxDecoration(
                                    color: PdfColor.fromHex('#204289'),
                                    borderRadius: const pw.BorderRadius.only(
                                      bottomLeft: const pw.Radius.circular(20),
                                      bottomRight: const pw.Radius.circular(20),
                                      topLeft: pw.Radius.circular(20),
                                      topRight: pw.Radius.circular(20),
                                    ),
                                  ),
                                  child: pw.Text(''),
                                ),

                                // arrow position

                                // box for profile
                              ],
                            )
                          ]))
                    ]))
          ]));

  return doc.save();
}

Future<void> saveAsFile(final BuildContext context, final LayoutCallback build,
    final PdfPageFormat pageFormat) async {
  final bytes = await build(pageFormat);
  final appDocDir = await getApplicationDocumentsDirectory();
  final appDocPath = appDocDir.path;

  final file = File('${appDocPath}/document.pdf');
  print('Save as file ${file.path}');

  await file.writeAsBytes(bytes);

  await OpenFile.open(file.path);
}

// Future<pw.PageTheme> _myPageTheme(PdfPageFormat format) async {
//   final logoImage = pw.MemoryImage(
//       (await rootBundle.load('assets/roojh_logo.png')).buffer.asUint8List());
//   // ignore: prefer_const_constructors
//   return pw.PageTheme(
//       margin: pw.EdgeInsets.symmetric(
//           horizontal: 1 * PdfPageFormat.cm, vertical: 0.5 * PdfPageFormat.cm),
//       textDirection: pw.TextDirection.ltr,
//       orientation: pw.PageOrientation.portrait,
//       buildBackground: (final context) => pw.FullPage(
//           ignoreMargins: true,
//           child: pw.Watermark(
//               angle: 20,
//               child: pw.Opacity(
//                   opacity: 0.5,
//                   child: pw.Image(logoImage,
//                       alignment: pw.Alignment.center, fit: pw.BoxFit.cover)))));
// }

void showPrintedToast(final BuildContext context) {
  ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Document Printed Successully')));
}

void showsharedToast(final BuildContext context) {
  ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Document Shared Successully')));
}
