import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class BloodPressureChart extends StatefulWidget {
  const BloodPressureChart({Key? key}) : super(key: key);

  @override
  State<BloodPressureChart> createState() => _BloodPressureChartState();
}

class _BloodPressureChartState extends State<BloodPressureChart> {
  List<_ChartData>? chartData;

  @override
  void initState() {
    chartData = <_ChartData>[
      _ChartData(2005, 132, 127),
      _ChartData(2006, 123, 118),
      _ChartData(2007, 132, 127),
      _ChartData(2008, 121, 116),
      _ChartData(2009, 124, 119),
      _ChartData(2010, 119, 115),
      _ChartData(2011, 126, 121),
      _ChartData(2012, 116, 111),
      _ChartData(2013, 132, 127),
      _ChartData(2014, 131, 126),
      _ChartData(2015, 128, 123),
      _ChartData(2016, 134, 129)
    ];
    super.initState();
  }

  Widget _buildLineChart() {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      title: ChartTitle(text: ''),
      primaryXAxis: NumericAxis(
          isVisible: false,
          edgeLabelPlacement: EdgeLabelPlacement.shift,
          interval: 2,
          majorGridLines: const MajorGridLines(width: 0)),
      primaryYAxis: NumericAxis(
        majorGridLines: MajorGridLines(
            width: 1, color: Colors.grey, dashArray: const <double>[5, 5]),
        interval: 10,
        maximum: 140,
        minimum: 110,
        axisLine: const AxisLine(
          width: 0,
        ),
        majorTickLines: const MajorTickLines(color: Colors.transparent),
      ),
      series: <LineSeries<_ChartData, num>>[
        LineSeries<_ChartData, num>(
            animationDuration: 1500,
            color: Color(0XFFF46524),
            dataSource: chartData!,
            xValueMapper: (_ChartData sales, _) => sales.sys,
            yValueMapper: (_ChartData sales, _) => sales.dia,
            width: 2,
            name: 'Hign',
            markerSettings: const MarkerSettings(isVisible: true)),
        LineSeries<_ChartData, num>(
            animationDuration: 1500,
            color: Color(0XFF204289),
            dataSource: chartData!,
            width: 2,
            name: 'Low',
            xValueMapper: (_ChartData sales, _) => sales.sys,
            yValueMapper: (_ChartData sales, _) => sales.time,
            markerSettings: const MarkerSettings(isVisible: true))
      ],
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 550,
      child: Container(
        color: Color(0XFFF6F1EE),
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Blood Pressure",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
                ),
                SizedBox(
                  width: 120,
                  child: DropdownButton<String>(
                    value: "Month",
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0XFF204289),
                    ),
                    elevation: 16,
                    style: const TextStyle(
                        color: Color(0XFF204289), fontSize: 30.0),
                    underline: Container(
                      height: 2,
                      color: Color(0XFF204289),
                    ),
                    onChanged: (String? newValue) {
                      setState(
                        () {
                          // update the state
                        },
                      );
                    },
                    items: <String>[
                      'Month',
                      'Week',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
            Text(
              "65/120",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25),
            ),
            _buildLineChart(),
          ],
        ),
      ),
    );
  }
}

class _ChartData {
  _ChartData(this.sys, this.dia, this.time);
  final double sys;
  final double dia;
  final double time;
}
