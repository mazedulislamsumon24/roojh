import 'dart:io';
import 'package:path_provider/path_provider.dart' as path_p;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:dio/dio.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';

class PdfView extends StatefulWidget {
  // ignore: non_constant_identifier_names
  var datas;

  PdfView({Key? key, this.datas}) : super(key: key);
  @override
  _PdfViewState createState() => _PdfViewState();
}

class _PdfViewState extends State<PdfView> {
  late PdfViewerController _pdfViewerController;
  @override
  bool _loading = false;
  Dio? dio;
  String? _fileFullPath;
  late int progress;
  void initState() {
    _pdfViewerController = PdfViewerController();
    dio = Dio();
    super.initState();
  }

  Future<List<Directory>?> _getExternalStoragePath() {
    return path_p.getExternalStorageDirectories(
        type: path_p.StorageDirectory.documents);
  }

  Future _downloadAndSaveFileToStorage(
      BuildContext context, String urlPath, String fileName) async {
    ProgressDialog pd = ProgressDialog(context: context);

    try {
      pd.show(max: 100, msg: 'File Downloading...');
      // final Directory _documentDir = Directory('/storage/emulated/0/RoojhDoc/$fileName');
      final dirList = await _getExternalStoragePath();
      final path = dirList![0].path;
      final file = File('$path/$fileName');
      await dio!.download(urlPath, file.path,
          onReceiveProgress: ((count, total) {
        setState(() {
          _loading = true;
          progress = ((count / total) * 100).toInt();
          print('$progress');
          pd.update(value: progress);
          // pr.update(message: "Please wait: ${progress!}");
        });
      }));
      _fileFullPath = file.path;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            backgroundColor: Colors.black,
            duration: Duration(seconds: 5),
            content: Text('download path: $_fileFullPath ')),
      );
    } catch (e) {
      pd.close();
      print(e);
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // final data = widget.datas!['pdfUrl'];
    return Scaffold(
        drawer: Drawer(),
        appBar: AppBar(
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: IconButton(
                  onPressed: () {
                    if (widget.datas.pdfUrl.toString() != "") {
                      _downloadAndSaveFileToStorage(
                          context,
                          widget.datas.pdfUrl.toString(),
                          '${DateTime.now()}.pdf');
                    } else {
                      _downloadAndSaveFileToStorage(
                          context,
                          widget.datas.imgUrl.toString(),
                          '${DateTime.now()}.jpg');
                    }
                  },
                  icon: Icon(Icons.download)),
            )
          ],
          iconTheme: const IconThemeData(color: Colors.black),

          backgroundColor: Colors.white,
          // leading: const Icon(Icons.access_alarms_outlined, color: Colors.black),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Center(
            child: Text(
              "View File",
              style: TextStyle(color: Colors.black),
            ),
          ),
        ),
        body: Container(
          child: widget.datas.pdfUrl.toString().length != 0
              ? SfPdfViewer.network(
                  "${widget.datas.pdfUrl.toString()}",
                  controller: _pdfViewerController,
                )
              : Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network(
                    "${widget.datas.imgUrl.toString()}",
                    width: double.infinity,
                    // height: 200,
                  ),
                ),
        ));
  }
}
