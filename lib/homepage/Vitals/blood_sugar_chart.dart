import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class BloodSugarChart extends StatefulWidget {
  const BloodSugarChart({Key? key}) : super(key: key);

  @override
  State<BloodSugarChart> createState() => _BloodSugarChartState();
}

class _BloodSugarChartState extends State<BloodSugarChart> {
  List<_SplineAreaData>? chartData;

  @override
  void initState() {
    chartData = <_SplineAreaData>[
      _SplineAreaData(2010, 21),
      _SplineAreaData(2011, 35),
      _SplineAreaData(2012, 19),
      _SplineAreaData(2013, 40),
      _SplineAreaData(2014, 60),
      _SplineAreaData(2015, 29),
      _SplineAreaData(2016, 60),
      _SplineAreaData(2017, 20),
      _SplineAreaData(2018, 55),
      _SplineAreaData(2019, 22),
      _SplineAreaData(2020, 33),
      _SplineAreaData(2021, 39),
      _SplineAreaData(2022, 44),
      _SplineAreaData(2023, 60),
      _SplineAreaData(2024, 51),
      _SplineAreaData(2025, 57),
      _SplineAreaData(2026, 51),
      _SplineAreaData(2027, 44),
      _SplineAreaData(2028, 22),
      _SplineAreaData(2029, 20),
      _SplineAreaData(2030, 55),
      _SplineAreaData(2031, 22),
      _SplineAreaData(2032, 33),
      _SplineAreaData(2033, 39),
      _SplineAreaData(2034, 44),
      _SplineAreaData(2035, 60),
      _SplineAreaData(2036, 51),
      _SplineAreaData(2037, 57),
      _SplineAreaData(2038, 51),
      _SplineAreaData(2039, 44),
      _SplineAreaData(2040, 22),
    ];
    super.initState();
  }

  Widget _buildSplineAreaChart() {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      primaryXAxis: NumericAxis(
          isVisible: false,
          interval: 10,
          majorGridLines: const MajorGridLines(width: 0),
          edgeLabelPlacement: EdgeLabelPlacement.shift),
      primaryYAxis: NumericAxis(
          labelFormat: '{value}',
          desiredIntervals: 10,
          maximum: 10,
          minimum: 60),
      series: <ChartSeries<_SplineAreaData, double>>[
        SplineAreaSeries<_SplineAreaData, double>(
          dataSource: chartData!,
          borderColor: Color(0xFFF46524),
          color: Color(0XFFFFAA33),
          borderWidth: 2,
          name: '',
          xValueMapper: (_SplineAreaData sales, _) => sales.time,
          yValueMapper: (_SplineAreaData sales, _) => sales.gLevel,
        )
      ],
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 550,
      child: Container(
        color: Color(0XFFF6F1EE),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Blood Sugar",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
                ),
                SizedBox(
                  width: 120,
                  child: DropdownButton<String>(
                    value: "Month",
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0XFF204289),
                    ),
                    elevation: 16,
                    style: const TextStyle(
                        color: Color(0XFF204289), fontSize: 30.0),
                    underline: Container(
                      height: 2,
                      color: Color(0XFF204289),
                    ),
                    onChanged: (String? newValue) {
                      setState(
                        () {
                          // update the state
                        },
                      );
                    },
                    items: <String>[
                      'Month',
                      'Week',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
            Text(
              "123 mmHG",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25),
            ),
            _buildSplineAreaChart(),
          ],
        ),
      ),
    );
  }
}

class _SplineAreaData {
  _SplineAreaData(
    this.time,
    this.gLevel,
  );
  final double time;
  final double gLevel;
}
