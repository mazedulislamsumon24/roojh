// import 'dart:html';

import 'package:animate_do/animate_do.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:roojh/Login_page/main_login.dart';
import 'package:roojh/common_code/topImg.dart';

import '../../pin_password/bio_authpage.dart';

class LoginWithPhone extends StatefulWidget {
  const LoginWithPhone({Key? key}) : super(key: key);

  @override
  _LoginWithPhoneState createState() => _LoginWithPhoneState();
}

class _LoginWithPhoneState extends State<LoginWithPhone> {
  //
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController controller = TextEditingController();
  final TextEditingController pin = TextEditingController();
  bool gotOtp = false;
  String verId = '';
  var phoneNo;
  bool _loading = false;
  bool codeSent = false;
  // by calling this function user can authenticate phone by otp and otp will be automatically
  // catched by phone and verify
  Future<void> verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNo.toString(), //user phone number with country code

        verificationCompleted: (PhoneAuthCredential credential) async {
          setState(() {
            gotOtp = true;
          });

          await Future.delayed(const Duration(seconds: 4), () {});
          await FirebaseAuth.instance.signInWithCredential(credential);
          final snackBar = SnackBar(content: Text("Login Success"));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => AuthPage()));
        },
        verificationFailed: (FirebaseAuthException e) {
          setState(() {
            _loading = false;
          });
          final snackBar = SnackBar(content: Text("${e.message}"));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        codeSent: (String? verficationId, int? resendToken) {
          setState(() {
            codeSent = true;
            verId = verficationId!;
          });
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          setState(() {
            verId = verificationId;
          });
        },
        timeout: Duration(seconds: 60));
  }

// if user get otp but his/her phone did not authenticate autmatically then
// user can type otp by himself
  Future<void> verifyPin(String pin) async {
    PhoneAuthCredential credential =
        PhoneAuthProvider.credential(verificationId: verId, smsCode: pin);

    try {
      await FirebaseAuth.instance.signInWithCredential(credential);
      const snackBar = SnackBar(content: Text("Login Success"));

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } on FirebaseAuthException catch (e) {
      final snackBar = SnackBar(content: Text("${e.message}"));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: codeSent //code sent will be true if otp has been sent to user's phone number
              // and this otp form will show up
              ? SizedBox(
                  height: 400,
                  child: gotOtp
                      ? Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              CircularProgressIndicator(),
                              Text('Loading Otp')
                            ],
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 40),
                          child: Column(
                            //for otp form confirmation
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'OTP Authentication',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Icon(Icons.phone_android_rounded)
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.shade900,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(23)),
                                ),
                                padding: EdgeInsets.only(
                                    left: 25, right: 25, top: 30, bottom: 25),
                                width: double.infinity,
                                height: 120,
                                child: Text(
                                  'Please verify your phone no , by entering OTP which is sent to your Phone No - ${phoneNo}',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 26.64),
                                child: Column(
                                  children: [
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'OTP',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              width: 140,
                                              height: 30,
                                              child: TextFormField(
                                                obscureText: true,
                                                enableSuggestions: false,
                                                autocorrect: false,
                                                style: const TextStyle(
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.w600),
                                                decoration: InputDecoration(
                                                  filled: true,
                                                  fillColor:
                                                      HexColor('#F3F6FF'),
                                                  border: InputBorder.none,
                                                  contentPadding:
                                                      const EdgeInsets.fromLTRB(
                                                          30.0,
                                                          10.0,
                                                          10.0,
                                                          15.0),
                                                  hintText: "Enter OTP",
                                                  hintStyle: const TextStyle(
                                                      fontSize: 14),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            98.67),
                                                    borderSide: BorderSide(
                                                      color:
                                                          HexColor('#CED3E1'),
                                                      width: 1.0,
                                                    ),
                                                  ),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      98.67),
                                                          borderSide:
                                                              BorderSide(
                                                            color: HexColor(
                                                                '#CED3E1'),
                                                            width: 1.0,
                                                          )),
                                                  errorStyle: TextStyle(
                                                      color: Colors.red,
                                                      fontSize: 15),
                                                  errorBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            98.67),
                                                  ),
                                                  focusedErrorBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            98.67),
                                                  ),
                                                ),
                                                controller: pin,
                                                validator: (value) {
                                                  if (value!.isEmpty) {
                                                    return 'please enter otp';
                                                  }
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 40),
                                              child: Container(
                                                height: 30,
                                                width: 90,
                                                child: ElevatedButton(
                                                  onPressed: () async {
                                                    verifyPin(pin.text);
                                                  },
                                                  child: Text('Verify',
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: Colors.white)),
                                                  style: ButtonStyle(
                                                      foregroundColor:
                                                          MaterialStateProperty
                                                              .all<Color>(
                                                                  Colors.black),
                                                      backgroundColor:
                                                          MaterialStateProperty
                                                              .all<
                                                                      Color>(
                                                                  HexColor(
                                                                      '#F46524')),
                                                      shape: MaterialStateProperty.all<
                                                              RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(25.9),

                                                        // side: BorderSide(color: Colors.red)
                                                      ))),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        Text('')
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )) //#####################
              //end of otp form
              : //######################################################################
              //if sendcode is not true then you will be in enter phone number form with
              Column(
                  children: [
                    TopImagesField(),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Form(
                        key: formKey,
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 2),
                                child: Text(
                                  'Phone No',
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 15),
                              decoration: BoxDecoration(
                                color: HexColor('#F3F6FF'),
                                borderRadius: BorderRadius.circular(98),
                                border: Border.all(color: HexColor('#CED3E1')),
                                //   boxShadow: [
                                //     BoxShadow(
                                //       color: Color(0xffeeeeee),
                                //       blurRadius: 10,
                                //       offset: Offset(0, 4),
                                //     ),
                                //   ],
                              ),
                              child: Stack(
                                children: [
                                  Container(
                                    height: 48,
                                    child: InternationalPhoneNumberInput(
                                      isEnabled: true,
                                      initialValue: PhoneNumber(
                                          dialCode: "+91", isoCode: "IN"),
                                      countrySelectorScrollControlled: true,
                                      onInputChanged: (PhoneNumber number) {
                                        print(number.phoneNumber);
                                      },
                                      onInputValidated: (bool value) {
                                        print(value);
                                      },
                                      selectorConfig: SelectorConfig(
                                        // setSelectorButtonAsPrefixIcon: false,
                                        showFlags: false,

                                        selectorType:
                                            PhoneInputSelectorType.BOTTOM_SHEET,
                                      ),
                                      errorMessage: 'Invalid Phone No',
                                      ignoreBlank: false,
                                      autoValidateMode:
                                          AutovalidateMode.disabled,
                                      textStyle: TextStyle(
                                          fontSize: 17.5,
                                          fontWeight: FontWeight.w600),
                                      selectorTextStyle: TextStyle(
                                          color: Colors.black, fontSize: 17.5),
                                      textFieldController: controller,
                                      formatInput: false,
                                      maxLength: 12,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              signed: true, decimal: false),
                                      cursorColor: Colors.black,
                                      inputDecoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                            bottom: 13, left: -30),
                                        border: InputBorder.none,
                                        hintText: 'Enter Your Phone Number',
                                        hintStyle: TextStyle(
                                            color: Colors.black,
                                            fontSize: 17.5,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      onSaved: (PhoneNumber number) {
                                        setState(() {
                                          phoneNo = number;
                                        });
                                        print('On Saved: $number');
                                      },
                                    ),
                                  ),
                                  Positioned(
                                    left: 50,
                                    top: 8,
                                    bottom: 8,
                                    child: Container(
                                      height: 40,
                                      width: 2,
                                      color: Colors.black.withOpacity(0.13),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(height: 80),
                            MaterialButton(
                              height: 51,
                              minWidth: double.infinity,
                              onPressed: () async {
                                formKey.currentState!.save();
                                print('clicked ${phoneNo}');
                                _loading = true;
                                await verifyPhone();
                                // _loading = false;
                              },
                              color: (HexColor('#F46524')),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(98)),
                              padding: EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 30),
                              child: _loading
                                  ? CircularProgressIndicator(
                                      color: Colors.white,
                                    )
                                  : Text(
                                      "Continue",
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
