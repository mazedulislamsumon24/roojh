import 'package:flutter/material.dart';
import 'package:roojh/common_code/profileTopImage.dart';
import 'package:roojh/homepage/Vitals/vitals.dart';

class VitalProfile extends StatefulWidget {
  const VitalProfile({Key? key}) : super(key: key);

  @override
  State<VitalProfile> createState() => _VitalProfileState();
}

class _VitalProfileState extends State<VitalProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TopProfileImage(),
          Padding(
            padding: const EdgeInsets.only(left: 26, right: 26),
            child: Column(
              children: [
                SizedBox(
                  height: 60,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Vitals',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  child: Vitals(),
                  height: 200,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
