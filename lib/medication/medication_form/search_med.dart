import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/medication/medication_form/meditationForm.dart';

class SearchMed extends StatefulWidget {
  const SearchMed({Key? key}) : super(key: key);

  @override
  State<SearchMed> createState() => _SearchMedState();
}

class _SearchMedState extends State<SearchMed> {
  QuerySnapshot? snapshotData;
  var getVal;
  bool? isexecuted = false;
  final TextEditingController searchController = TextEditingController();
  Widget searchData() {
    return ListView.builder(
      itemCount: snapshotData!.docs.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          onTap: () {
            getVal = (snapshotData?.docs[index].data() as dynamic)['med_name'];
            var company =
                (snapshotData?.docs[index].data() as dynamic)['company'];
            print(getVal);
            // Navigator.of(context).push(MaterialPageRoute(
            //     builder: (context) =>
            //         MeditationForm(getVal: getVal, company: company)));
          },
          title: Text(
            "${(snapshotData?.docs[index].data() as dynamic)['med_name']}",
            style: TextStyle(fontSize: 16),
          ),
          subtitle: Text(
            "${(snapshotData?.docs[index].data() as dynamic)['company']}",
            style: TextStyle(fontSize: 13),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: HexColor("#F46524"),
          actions: [
            GetBuilder<DataController>(
              init: DataController(),
              builder: (val) {
                return IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    val.queryData(searchController.text).then((value) {
                      setState(() {
                        snapshotData = value;
                        isexecuted = true;
                      });
                    });
                  },
                );
              },
            )
          ],
          title: TextField(
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              hintText: 'Search Medicine',
              hintStyle: TextStyle(color: Colors.white),
            ),
            controller: searchController,
          ),
        ),
        body: isexecuted == true
            ? searchData()
            : Center(
                child: Container(),
              ));
  }
}

class DataController extends GetxController {
  Future getData(String collection) async {
    final FirebaseFirestore firebaseStore = FirebaseFirestore.instance;
    QuerySnapshot snapshot = await firebaseStore.collection(collection).get();
    return snapshot.docs;
  }

  Future queryData(String queryString) async {
    return await FirebaseFirestore.instance
        .collection('med_datas')
        .where('med_name', isGreaterThanOrEqualTo: queryString)
        .get();
  }
}
