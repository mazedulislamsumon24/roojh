import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:roojh/forget_password/forgetPassword/confirmation_msg.dart';

class ByEmail extends StatefulWidget {
  const ByEmail({Key? key}) : super(key: key);

  @override
  State<ByEmail> createState() => _ByEmailState();
}

class _ByEmailState extends State<ByEmail> {
  FirebaseAuth? user = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  bool confirm = false;
  final emailController = TextEditingController();
  Future<void> resetPassword(email) async {
    try {
      await user!.sendPasswordResetEmail(email: email);
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => const ResetPasswordSent()));
    } on FirebaseAuthException catch (e) {
      print(e.code);
      print(e.message);
// show the snackbar here
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            const Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(bottom: 2),
                child: Text(
                  'Email id',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
              decoration: InputDecoration(
                filled: true,
                fillColor: HexColor('#F3F6FF'),
                border: InputBorder.none,
                contentPadding:
                    const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                hintText: "Enter Your Email Id",
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(98.67),
                  borderSide: BorderSide(
                    color: HexColor('#CED3E1'),
                    // width: 2.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(98.67),
                    borderSide: BorderSide(
                      color: HexColor('#CED3E1'),
                      width: 1.0,
                    )),
                errorStyle: const TextStyle(color: Colors.red, fontSize: 15),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(98.67),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(98.67),
                ),
              ),
              controller: emailController,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'please enter name';
                }
              },
            ),
            const SizedBox(
              height: 19,
            ),
            const Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 9),
                child: Text(
                  'We are sending authentication link on above Id',
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                  // textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(height: 80),
            Container(
              height: 51.8,
              width: MediaQuery.of(context).size.width,
              child: ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    setState(() {
                      var email = emailController.text;
                      print(email);
                      resetPassword(email.toString().trim());
                      // final SharedPreferences sharedPreferences =
                      //     await SharedPreferences.getInstance();
                      // sharedPreferences.setString(
                      //     'email', emailController.text);
                      // Navigator.pushNamed(context, "/auth");
                    });
                    // _formKey.currentState!.save();

                    // print(_loginUserData);
                    // AuthController.login(_loginUserData);
                    // Navigator.pushNamed(context, "/auth");
                  }
                },
                child: const Text('Continue',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(HexColor('#F46524')),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.9),

                      // side: BorderSide(color: Colors.red)
                    ))),
              ),
            )
          ],
        ));
  }
}
