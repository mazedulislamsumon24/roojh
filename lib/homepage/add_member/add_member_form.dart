import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:roojh/homepage/upload_file/view/moreUpload.dart';

class AddMemberForm extends StatefulWidget {
  const AddMemberForm({Key? key}) : super(key: key);

  @override
  State<AddMemberForm> createState() => _AddMemberFormState();
}

class _AddMemberFormState extends State<AddMemberForm> {
  final FirebaseStorage _storageRef = FirebaseStorage.instance;
  late File _image;
  final _genderOptions = [
    "Male",
    "Female",
    "Others",
    "Prefer not to disclose",
  ];
  final user = FirebaseAuth.instance.currentUser; // get user details

  Future<void> uploadImage(XFile _image) async {
    Reference reference =
        _storageRef.ref().child('${user?.email}').child(_image.name);
    UploadTask uploadTask = reference.putFile(File(_image.path));
    // ################################################
    // it will show uploading notification on screen
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
          backgroundColor: Colors.black,
          duration: Duration(seconds: 5),
          content: Text('Uploading')),
    );
    // ##################################
    // when upload is successful then it will show a notification that upload successful
    await uploadTask.whenComplete(() {
      print(reference.getDownloadURL());
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            backgroundColor: Colors.black,
            duration: Duration(seconds: 2),
            content: Text('Upload Successful')),
      );
    });
  }

  Future getImage() async {
    var image = await ImagePicker().pickImage(source: ImageSource.camera);
    // if image is not selected then it will return null
    if (image == null) return;
    // ###########################
    // if image captured
    var filename = image.name; //get image name
    // print('file name--- $filename');
    // final imageTemporary = await File(image.path);
    // print('file name--- $imageTemporary');
    // setState(() {
    //   // this._image = imageTemporary;
    // });

    if (image != null) {
      // ############################
      //upload image function called after capturing image
      await uploadImage(image);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 17.0, right: 17, top: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        "Add Member",
                        style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                      MaterialButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        color: const Color(0xffd4e1f4),
                        textColor: Colors.white,
                        child: const Icon(
                          Icons.clear,
                          size: 22,
                          color: Colors.black,
                        ),
                        padding: const EdgeInsets.all(0),
                        shape: const CircleBorder(),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Color(0xFFCDDAFF),
                        radius: 50,
                        child: Icon(
                          Icons.camera_alt_outlined,
                          size: 40,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(12),
                          primary: Color(0xFF204289),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(21.0),
                          ),
                        ),
                        onPressed: () {},
                        child: Text(
                          "Upload Picture",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "User Name",
                        style: TextStyle(
                            color: Colors.grey.shade600,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        height: 52,
                        child: TextField(
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Color(0xFFF3F6FF),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(127.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 0.5),
                                borderRadius: BorderRadius.circular(127),
                              ),
                              hintText: "  Full Name"),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Age",
                        style: TextStyle(
                            color: Colors.grey.shade600,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        height: 51,
                        child: TextField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Color(0xFFF3F6FF),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(127.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 0.5),
                                borderRadius: BorderRadius.circular(127.0),
                              ),
                              hintText: "  Enter Age"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Gender",
                        style: TextStyle(
                            color: Colors.grey.shade600,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        height: 49,
                        child: FormField<String>(
                          builder: (FormFieldState<String> state) {
                            return InputDecorator(
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0xFFF3F6FF),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(127.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: 0.5),
                                    borderRadius: BorderRadius.circular(127.0),
                                  ),
                                  hintText: "  Select Gender"),
                              isEmpty: true,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  // value: "Male",
                                  isDense: true,
                                  onChanged: (newValue) {
                                    print(newValue);
                                  },
                                  items: _genderOptions.map((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    // width: ,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        // ############################
                        // File upload button
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => UploadFileList()));
                            },
                            child: SvgPicture.asset(
                              'icons/fileupload2.svg',
                              width: 130,
                              height: 76,
                            )),
                        // ############################
                        // Capture image button
                        TextButton(
                            onPressed: () async {
                              await getImage(); //calling getimage function which will capture image and ulpad to storage
                            },
                            child: SvgPicture.asset(
                              'icons/uploadImage.svg',
                              width: 130,
                              height: 76,
                            )),
                        // SizedBox()
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CheckboxListTile(
                    title: Text(
                      "I have full permissions to upload family members document",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                    ),
                    value: false,
                    onChanged: (newValue) {
                      print(newValue);
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.all(12),
                        primary: Color(0xFFF46524),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "Save",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
